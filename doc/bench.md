# Benchmark results

> This file will be populated when the project has been deemed suitable for beta production use. 

> The current results are for comparison only. Results are calculated using the `desktop` call sign system only.

Benchmark results should help the reader get a feeling of the expected performance of the syslog parser. In this
benchmark I offer multiple system configurations and some unlikely configurations that were used during 
development. Benchmarks might give the reader insight into expected parsing performance in number of messages processed
per second, or how it compares to other syslog parser libraries.

## Comparable libraries

- [go-syslog](https://github.com/influxdata/go-syslog) by [InfluxData](https://www.influxdata.com/): a *blazing fast* 
syslog parser powered by a [ragel](https://pkg.go.dev/github.com/biogo/ragel) generated finite state machine. 
For the remainder of this document, the InfluxData go-syslog parser will be referred to as `influx`. 

## Systems

Multiple systems has been used to benchmark this library and comparable libraries. Three hardware platforms have been
used during the benchmark: a laptop, a desktop computer and one enterprise grade server.

> **NOTE**: Neither hardware platforms use contemporary hardware components, but outdated, early
> 2010s, hardware components.

*I will try to get my hands on some modern computer hardware to bench the library. Most likely it will suffice with a 
relatively modern enterprise grade server platform.*

|Call sign|System|CPU|RAM|OS|
|---|---|---|---|---|
|laptop|Lenovo ThinkPad T430|Intel i5|8GB DDR3|Manjaro Linux|
|desktop|Desktop computer|Intel i7-4930K|16GB DDR3 1333 MHz|Windows 10|
|*See below*|HPE ProLiant DL380 G8|2x Intel X5675|96GB DDR3|VMWare ESXi 6.7|

### Server benchmarks

The idea of using a massively over scaled enterprise grade server is benchmarking on differently configured virtual 
machines. It is very likely that syslog servers will be running on virtual machines. ESXi 6.7 is the latest 
officially supported ESXi version for the HPE ProLiant DL380 G8 server family. I propose the following virtual machine 
configurations:

|Call sign|CPU|RAM|OS|
|---|---|---|---|
|vmMin|1|1GB|CentOS 8|
|vmMin+|2|1GB|CentOS 8|
|vmMed|2|2GB|CentOS 8|
|vmMed+|4|2GB|CentOS 8|
|vmMax|4|4GB|CentOS 8|

## Results

Benchmark|System call sign|n|Result (ns/op)|Throughput (msg/s)|
|---|---|---|---|---|
|5424Minimal                            |`desktop`|3381381  |351    |2,849,002.85|
|5424MinTimestamp                       |`desktop`|1490982  |806    |1,240,694.79|
|5424StructuredData                     |`desktop`|857295   |1321   |757,002.27|
|5424MassiveDataNoStructured            |`desktop`|800389   |1493   |669,792.36|
|5424MassiveData                        |`desktop`|444567   |2735   |365,630.71|
|5424MaxDataNoStructuredDataNoMessage   |`desktop`|545630   |2231   |448,229.49|
|5424MaxDataNoMessage                   |`desktop`|50016    |23996  |41,673.61|
|5424MaxDataNoStructuredData            |`desktop`|545593   |2255   |443,458.98|
|5424MaxData                            |`desktop`|49807    |24185  |41,347.94|
||||||
|3164Minimal                            |`desktop`|5096346  |234    |4,273,504.27|
|3164MinimalTimestamp                   |`desktop`|1579471  |766    |1,305,483.03|
|3164MaxData                            |`desktop`|1249603  |952    |1,050,420.17|
||||||
|5424MaxDataNoStructuredDataOC          |`desktop`|255332   |4736   |211,148.65|
|5424MaxDataOC                          |`desktop`|24601    |48577  |20,585.87|
|3164MaxDataOC                          |`desktop`|600038   |1979   |505,305.71|

*Results: **ns/op** means nanoseconds per loop iteration. Lower is better.*

*Throughput: **msg/s** means messages processed per second with the throughput of the performance results.
Higher is better.*

It is clear that RFC5424 suffers greatly from parsing structured data elements. An interesting observation is
the difference, or lack thereof, between *5424MaxDataNoStructuredDataNoMessage* and *5424MaxDataNoStructuredData*. 
The addition of a long message does not significantly affect parser performance. This is because parsing the message
part is an O(1) operation; the parser just tokenizes the remaining length of the current message as the message part.
