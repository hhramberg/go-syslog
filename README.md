# Go Syslog

The `go-syslog` module provides syslog parsing for both [RFC3164](https://datatracker.ietf.org/doc/html/rfc3164) and
[RFC5424](https://datatracker.ietf.org/doc/html/rfc5424) compatible syslog messages. Parsing multiple syslog messages
in a single input string is supported out of the box, no extra configuration required, using 
[RFC6587](https://datatracker.ietf.org/doc/html/rfc6587) 
[octet counting](https://datatracker.ietf.org/doc/html/rfc6587#section-3.4.1).

## Description

[//]: <> (Let people know what your project can do specifically. Provide context and add a link to any reference 
visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. 
If there are alternatives to your project, this is a good place to list differentiating factors.)

The `go-syslog` module gives Go programmers a library for parsing a syslog message from a source string.

> In computing, syslog (...) is a standard for message logging. It allows separation of the software that generates 
> messages, the system that stores them, and the software that reports and analyzes them.
> [Wikipedia](https://en.wikipedia.org/wiki/Syslog).

An example RFC3164 syslog message:

```text
<34>Oct 11 22:14:15 mymachine.example.com su: 'su root' failed for lonvick on /dev/pts/8
```

The same example syslog message in RFC5424 format:

```text
<34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - - - 'su root' failed for lonvick on /dev/pts/8
```

A syslog message contains, among other things, a priority field `<34>`, a timestamp `2003-10-11T22:14:15.003Z`/`Oct 11 22:14:15`, 
the originator of the message `mymachine.example.com` and the message itself 
`'su root' failed for lonvick on /dev/pts/8`.

*This is not a syslog server, collector or relay. This module is merely a parser. Your implementation will need to collect syslog data before using this module to parse the data into a structured syslog message for use in any Go application.*

### Existing libraries

- [go-syslog](https://github.com/influxdata/go-syslog) by [InfluxData](https://www.influxdata.com/): a *blazing fast* syslog parser powered by a [ragel](https://pkg.go.dev/github.com/biogo/ragel) generated finite state machine.
  For the remainder of this document, the InfluxData go-syslog parser will be referred to as `influx`.
- [syslogparser](https://github.com/jeromer/syslogparser) by [Jérôme Renard](https://github.com/jeromer).

## Installation

Having the `go` executable installed and `GOPATH` set you may issue the following command to download the go-syslog
module to your system.

```shell
go get gitlab.com/hhramberg/go-syslog
```

Now you may use the go-syslog module in any project given that your `GOPATH` doesn't change.

```go
import slog "gitlab.com/hhramberg/go-syslog/src"
```

You can now reference the Go module functions and other resources. In the above example the `slog` alias is used in place of the full module name.

## Usage

The syslog parser automatically detects syslog version.

```go
import slog "gitlab.com/hhramberg/go-syslog/src"

data := "<165>Oct 11 12:34:56 db02-datastore hard disks are nearly full, usage is 90%"

if rec, err := slog.Parse(data); err == nil {
    fmt.Println(rec[0].String())
}

// Output:
// Version: RFC3164 (0), Timestamp: 0000-10-11T12:34:56Z, Facility: local use 4 (20), Severity: Notice (5), Host: "db02-datastore", Message: "hard disks are nearly full, usage is 90%"

```

The parser returns a `[]SyslogRecord` slice on success. On a failed parse, an error is returned. 
`SyslogRecord` is defined below.

> The parser returns a `[]SyslogRecord` slice because the parser supports parsing of multiple messages in a single data stream using [RFC6587](https://datatracker.ietf.org/doc/html/rfc6587)
[octet counting](https://datatracker.ietf.org/doc/html/rfc6587#section-3.4.1).

Note that the `go-syslog` parser parses syslog messages strictly in compliance with either
[RFC3164](https://datatracker.ietf.org/doc/html/rfc3164) or
[RFC5424](https://datatracker.ietf.org/doc/html/rfc5424). If the message does not conform to either standard, the parser returns an `error`. There is no *best effort* mode.

```go
type SyslogRecord struct {
    Version   int
    Facility  int
    Severity  int
    Timestamp time.Time
    Host      string
    AppName   string
    ProcessId string
    MessageId string
    Message   string
    Data      map[string]StructuredData
}
```

The `SyslogRecord.Data` field contains a map of `StructuredData` elements, as defined in [RFC5424](https://datatracker.ietf.org/doc/html/rfc5424). An element has an id and a slice of key-value pairs called `StructuredDataParam`.

```go
type StructuredData struct {
    Id     string
    Params []StructuredDataParam
}

type StructuredDataParam struct {
    Key string
    Val string
}
```

`StructuredDataParam` is a key-value pair structure where both key and value are strings.

## Support

Create a new *issue* by clicking the New button in the top navigation panel. I will address the issue as fast as I am
noticed by the GitLab notification system.

## Roadmap

Below is a short list of future and past additions to the `go-syslog` parser.

- [x] Test all RFC3164 and RFC5424 message permutations. Testing all permutations is a daunting task, but the current suite of test tests cover *many* of them.
- [x] Benchmark the ASCII-version of the lexer. A previous version of the parses was fully UTF-8 capable, which was overkill. Syslog messages are initially ASCII character streams with some parts being UTF-8 compatible.
- [x] Add validation for structured data elements that use the IANA-registered SD-IDs, per 
[RFC5424 section 7](https://datatracker.ietf.org/doc/html/rfc5424#section-7). This feature would be an *optional* feature that can be called after initial parsing.
- [ ] Benchmark parser on multiple systems. It is desirable to benchmark on virtual machines because these platforms are suitable for syslog collection services and provides an easy way to scale hardware resources in an inexpensive way.
- [ ] Benchmark this parser versus similar syslog parsers.
- [x] Add [octet counting](https://datatracker.ietf.org/doc/html/rfc6587#section-3.4.1)
  per [RFC6587](https://datatracker.ietf.org/doc/html/rfc6587).
- [ ] Add [non-transparent framing](https://datatracker.ietf.org/doc/html/rfc6587#section-3.4.2)
  per [RFC6587](https://datatracker.ietf.org/doc/html/rfc6587). I'm currently reluctant to add this feature as it will *greatly* affect performance of parsing the *message* part of a syslog message. The *message* part is currently being tokenized in O(1) time. Transparent framing will make *message* tokenization O(n) because the *message* must be iterated until a break character is encountered.
- [ ] Add vendor decoding. Some vendors, such as Cisco, pre-pends the syslog message part. These prefixes can
  be stripped for information and put into structured data elements which would make messages more specific and compact.
  - [x] Cisco
  - [ ] Common Event Format (CEF)
  - [ ] Palo Alto
  - [ ] Sonicwall
- [x] Add non-vendor specific decoding. Store in structured data under id 'key-value'.
  - [x] Key-value pairs using ':' as separator.
  - [x] Key-value pairs using '=' as separator.

[//]: <> (## Contributing)

[//]: <> (State if you are open to contributions and what your requirements are for accepting them.)

[//]: <> (For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.)

[//]: <> (You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.)

## Acknowledgments

The lexer is inspired heavily by Rob Pike's [talk](https://www.youtube.com/watch?v=HxaD_trXwRE)
on lexical scanning in Go from 2011. The slides can be found [here](https://talks.golang.org/2011/lex.slide#1).

The [influx syslog parser](https://github.com/influxdata/go-syslog) project provided inspiration for writing better unit tests and benchmarks.

## License

This project is licensed under the MIT licence. See [LICENSE](LICENSE) for more information. As a personal note, *you may freely use this software in your project, be they open source or monetised projects*.

## Project status

This project is developed on hobby basis. The main ambition is to first implement basic RFC3164 and RFC5424 parsing. After the basics have been covered I plan to add more features, as stated in the *Roadmap* section.

## Benchmark

[bench.md](doc/bench.md) lists parser performance benchmarks for different system configurations and message types.
