# Change Log

## [0.6.0] November 6th 2022
### Added
Cisco vendor decoding. Given the example syslog message below, the Cisco vendor decoder `src.DecodeCisco` searches the `src.SyslogRecord.Message` part for Cisco specific information. The information is stored in the `src.SyslogRecord.StructuredData` hash map. 

```
Oct 14 09:38:48 souter1: %SYS-5-CONFIG_I: Configured from console by console
```

The above example message will generate a new entry in the `src.SyslogRecord.StructuredData` with ID `vendor` looking like this.

```go
package main

import (
	"fmt"
	slog "gitlab.com/hhramberg/go-syslog/src"
)

func main() {
	msg := "Oct 14 09:38:48 souter1: %SYS-5-CONFIG_I: Configured from console by console"
	records, _ := slog.Parse(msg)
	r := records[0]
	fmt.Println(r.Message)
	slog.DecodeCisco(&r)
	fmt.Println(r.Message)
	for k, v := range r.Data {
		fmt.Printf("key: %q, value:\n\t%+v\n", k, v)
	}
}

// Output:
//
// %SYS-5-CONFIG_I: Configured from console by console
// Configured from console by console
// key: "vendor", value:
//      {Id:vendor Params:[{Key:name Val:Cisco} {Key:facility Val:SYS} {Key:mnemonic Val:CONFIG_I}]}
```

### Changed
`SyslogRecord.Data` is now a hash map, not a slice. This change was introduced to reflect the requirements in [RFC5424, section 6.3.2](https://datatracker.ietf.org/doc/html/rfc5424#section-6.3.2) of non repeating structured data IDs.

### Fixed
