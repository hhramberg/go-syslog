// parser.go contains code for parsing tokens from the lexer and deciding whether the token stream
// is a valid RFC5424 or RFC3164 syslog message. A valid token stream returns a syslog record.

package src

import (
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"
)

// ----------------------------
// ----- Type definitions -----
// ----------------------------

// StructuredDataParam defines a key-value pair per RFC5424.
type StructuredDataParam struct {
	Key string // Structured data key.
	Val string // Structured data value.
}

// StructuredData defines a set of generic Key-value pairs per RFC5424.
type StructuredData struct {
	Id     string                // ID of this structured data element.
	Params []StructuredDataParam // Slice of Key-value pairs.
}

// SyslogRecord holds a syslog message data. It can be an RFC5424 or RFC3164 syslog record,
// denoted by its Version field.
type SyslogRecord struct {
	Version   int                       // Version defines the Syslog version. 0 = RFC3164, 1 = RFC5424.
	Facility  int                       // Facility defines the facility of message. In range [0, 23].
	Severity  int                       // Severity defines the severity of message. In range [0, 7].
	Timestamp time.Time                 // Timestamp defines the timestamp when message was sent from the exporter.
	Host      string                    // Host defines the optional hostname or IP address of the exporter.
	AppName   string                    // AppName defines the optional source process name of the exporter that generated the message.
	ProcessId string                    // ProcessId defines the optional OS process ID (PID) of originating process on the exporter.
	MessageId string                    // MessageId defines the optional RFC5424 type of message for semantic purposes.
	Message   string                    // Message holds syslog message.
	Data      map[string]StructuredData // Data holds RFC5424 structured data or vendor decoded data.
}

// ---------------------
// ----- Constants -----
// ---------------------

const RFC3164 = 0 // RFC3164 identifies that a SyslogRecord represents an RFC3164 syslog message.
const RFC5424 = 1 // RFC5424 identifies that a SyslogRecord represents an RFC5424 syslog message.

// --------------------
// ----- Globals ------
// --------------------

// versions provides string literals for syslog versions.
var versions = [...]string{
	"RFC3164",
	"RFC5424",
}

// facilities provides string literals for syslog facility values.
var facilities = [...]string{
	"kernel",
	"user-level",
	"mail system",
	"system daemons",
	"security/authorization",
	"internal syslogd",
	"line printer subsystem",
	"network news subsystem",
	"UUCP subsystem",
	"clock daemon",
	"security/authorization",
	"FTP daemon",
	"NTP subsystem",
	"log audit",
	"log alert",
	"clock daemon",
	"local use 0",
	"local use 1",
	"local use 2",
	"local use 3",
	"local use 4",
	"local use 5",
	"local use 6",
	"local use 7",
}

// severities provides string literals for syslog severity values.
var severities = [...]string{
	"Emergency",
	"Alert",
	"Critical",
	"Error",
	"Warning",
	"Notice",
	"Informational",
	"Debug",
}

// tDesc provides print friendly descriptions of tokenType names.
var tDesc = [...]string{
	"end of input",
	"error",
	"-",
	"priority",
	"version",
	"timestamp",
	"timestamp sub second",
	"message",
	"hostname",
	"application name",
	"process id",
	"message id",
	"structured data id",
	"structured data key",
	"structured data value",
	"begin",
	"end",
	"octet count identifier",
	"integer",
	"alphabetic character",
	"alphanumeric character",
	"string literal",
	"vendor tag - Cisco facility",
	"vendor tag - Cisco sub facility",
	"vendor tag - Cisco severity",
	"vendor tag - Cisco mnemonic",
}

// ----------------------
// ----- Functions ------
// ----------------------

// Parse attempts to parse one or more RFC5424 or a single RFC3164 compatible syslog message, and returns a slice
// of SyslogRecord on success. An error is returned if the input data stream is not a valid syslog message.
//
// The Parse function returns a slice to keep the function from changing signature for future features such as
// octet counting and non-transparent framing where multiple syslog messages can be aggregated into a single
// message stream.
func Parse(s string) ([]SyslogRecord, error) {
	if len(s) < 1 {
		return nil, errors.New("message length is zero")
	}

	res := make([]SyslogRecord, 0, 1)
	l := lexer{
		input: s,
		line:  1,
		end:   len(s),
		state: lexOctetCount,
	}

	for t := l.nextToken(); t.typ != tokenEOF; t = l.nextToken() {
		sr := SyslogRecord{Facility: 1, Severity: 5} // Set default priority.

		if t.typ == tokenOctetCount {
			// Octet counting; consume token. Lexer keeps track of source string index.
			t = l.nextToken()
		}
		switch t.typ {
		case tokenPri:
			// Can be either RFC3164 or RFC5424.
			i := unsafeAtoi(t.val)
			if i < 0 || i > 191 {
				return nil, fmt.Errorf("line %d:%d: invalid priority value %d", t.line, t.pos, i)
			}
			sev := i % 8
			sr.Facility = (i - sev) >> 3
			sr.Severity = sev
		case tokenTimestamp, tokenTimestampSubSecond:
			// Must be RFC3164.
			if err := parseTimestamp3164(t, &l, &sr); err != nil {
				return nil, err
			}
			res = append(res, sr)
			continue
		case tokenHostname:
			// Must be RFC3164.
			if err := parseHostname3164(t, &l, &sr); err != nil {
				return nil, err
			}
			res = append(res, sr)
			continue
		case tokenMessage:
			// Must be RFC3164.
			if err := parseMessage3164(t, &l, &sr); err != nil {
				return nil, err
			}
			res = append(res, sr)
			continue
		default:
			return nil, fmt.Errorf("line %d:%d: expected %s, %s, %s or %s, got %s",
				t.line, t.pos, tDesc[tokenPri], tDesc[tokenTimestamp], tDesc[tokenHostname], tDesc[tokenMessage], t.Type())
		}

		t = l.nextToken()
		switch t.typ {
		case tokenVersion:
			// Must be RFC5424.
			i, err := strconv.Atoi(t.val)
			if err != nil {
				return nil, fmt.Errorf("line %d:%d: version %q is not an integer", t.line, t.pos, t.val)
			}
			if i != 1 {
				return nil, fmt.Errorf("line %d:%d: invalid version %d", t.line, t.pos, i)
			}
			sr.Version = RFC5424
			if err := parseRFC5424(&l, &sr); err != nil {
				return nil, err
			}
		case tokenTimestamp:
			// Must be RFC3164.
			if err := parseTimestamp3164(t, &l, &sr); err != nil {
				return nil, err
			}
			res = append(res, sr)
			continue
		case tokenHostname:
			// Must be RFC3164.
			if err := parseHostname3164(t, &l, &sr); err != nil {
				return nil, err
			}
			res = append(res, sr)
			continue
		case tokenMessage:
			// Must be RFC3164.
			if err := parseMessage3164(t, &l, &sr); err != nil {
				return nil, err
			}
			res = append(res, sr)
			continue
		case tokenEOF:
			// Must be RFC3164.
			// RFC3164 permits EOF in the second token, indicating a non-empty message.
			// This break statement is unnecessary, but it signals to the interested
			// reader that the parsing will terminate.
			break
		default:
			if t.typ == tokenError {
				return nil, errors.New(t.val)
			}
			return nil, fmt.Errorf("line %d:%d: expected %s, %s, %s or %s, got %s",
				t.line, t.pos, tDesc[tokenVersion], tDesc[tokenTimestamp], tDesc[tokenHostname], tDesc[tokenMessage], tDesc[t.typ])
		}
		res = append(res, sr)
	}
	return res, nil
}

// String returns a print friendly string representation of SyslogRecord sr.
func (sr *SyslogRecord) String() string {
	return fmt.Sprintf("Version: %s (%d), Timestamp: %s, Facility: %s (%d), Severity: %s (%d), Host: %q, Message: %q",
		versions[sr.Version], sr.Version, sr.Timestamp.Format(time.RFC3339Nano), facilities[sr.Facility], sr.Facility, severities[sr.Severity], sr.Severity,
		sr.Host, sr.Message)
}

// StringRFC3164 returns the RFC3164 string representation of SyslogRecord sr.
func (sr *SyslogRecord) StringRFC3164() string {
	return fmt.Sprintf("<%d>%s %s %s[%s]: %s", sr.Severity*8+sr.Facility, sr.Timestamp.Format(time.StampNano), sr.Host, sr.AppName, sr.ProcessId, sr.Message)
}

// StringRFC5424 returns the RFC5424 string representation of SyslogRecord sr.
func (sr *SyslogRecord) StringRFC5424() string {
	sb := strings.Builder{}
	sb.Grow(10 + len(sr.Host) + len(sr.AppName) + len(sr.ProcessId) + len(sr.MessageId) + len(sr.Message))
	sb.WriteByte('<')
	sb.WriteString(strconv.Itoa(sr.Severity*8 + sr.Facility))
	sb.WriteByte('>')
	sb.WriteByte(' ')
	sb.WriteString(sr.Timestamp.Format(time.RFC3339Nano))
	sb.WriteByte(' ')
	if len(sr.Host) > 0 {
		sb.WriteString(sr.Host)
	} else {
		sb.WriteByte('-')
	}
	sb.WriteByte(' ')
	if len(sr.AppName) > 0 {
		sb.WriteString(sr.AppName)
	} else {
		sb.WriteByte('-')
	}
	sb.WriteByte(' ')
	if len(sr.ProcessId) > 0 {
		sb.WriteString(sr.ProcessId)
	} else {
		sb.WriteByte('-')
	}
	sb.WriteByte(' ')
	if len(sr.MessageId) > 0 {
		sb.WriteString(sr.MessageId)
	} else {
		sb.WriteByte('-')
	}
	sb.WriteByte(' ')
	if len(sr.Data) > 0 {
		for _, v := range sr.Data {
			sb.WriteByte('[')
			sb.WriteString(v.Id)
			for _, elem := range v.Params {
				sb.WriteByte(' ')
				sb.WriteString(elem.Key)
				sb.WriteByte('=')
				sb.WriteString(fmt.Sprintf("%q", elem.Val))
			}
			sb.WriteByte(']')
		}
	} else {
		sb.WriteByte('-')
	}
	sb.WriteByte(' ')
	if len(sr.Message) > 0 {
		sb.WriteString(sr.Message)
	} else {
		sb.WriteByte('-')
	}
	return sb.String()
}

// VerifyIANA verifies any IANA reserved SD-ID for SyslogRecord sr,
// if it is an RFC5424 syslog message. If the record is RFC5424 compliant and
// SD-ID assigned by IANA are compliant by IANA standards, per RFC5424 section 7,
// nil is returned. If the SD-ID are not compliant, an error is returned.
func VerifyIANA(sr *SyslogRecord) error {
	if sr.Version != RFC5424 || len(sr.Data) == 0 {
		return nil
	}

	// RFC5424 section 7.1.
	if tz, ok := sr.Data["timeQuality"]; ok {
		if len(tz.Params) == 0 {
			return errors.New("optional IANA defined structured data id 'timeQuality' defined, but no key-value pair parameters provided")
		}
		isSynced := true
		for _, e1 := range tz.Params {
			switch e1.Key {
			case "tzKnown":
				if len(e1.Val) != 1 {
					return errors.New("expected exporter to know timezone, got neither")
				}
				if e1.Val[0] != '0' && e1.Val[0] != '1' {
					return errors.New("expected exporter to know timezone, got neither")
				}
			case "isSynced":
				if len(e1.Val) != 1 {
					return errors.New("expected exporter to know timezone, got neither")
				}
				if e1.Val[0] == '0' {
					isSynced = false
					continue
				}
				if e1.Val[0] != '1' {
					return errors.New("expected exporter to know timezone, got neither")
				}
			case "syncAccuracy":
				if !isSynced {
					return errors.New("got syncAccuracy details, but exporter reported no isSync")
				}
				if _, err := strconv.Atoi(e1.Val); err != nil {
					return fmt.Errorf("expected integer for syncAccuracy, got %s", e1.Val)
				}
			default:
				return fmt.Errorf("expected IANA specified structured data parameter key 'tzKnown', 'isSynced' or 'syncAccuracy', got %s", e1.Val)
			}
		}
	}

	// RFC5424 section 7.2.
	if origin, ok := sr.Data["origin"]; ok {
		if len(origin.Params) == 0 {
			return errors.New("optional IANA defined structured data id 'origin' defined, but no key-value pair parameters provided")
		}
		for _, e1 := range origin.Params {
			switch e1.Key {
			case "ip":
				if ip := net.ParseIP(e1.Val); ip == nil {
					return fmt.Errorf("expected IP address in parameter with key 'ip', got %s", e1.Val)
				}
			case "enterpriseId":
				// Must have a leading SMI of iso.org.dod.internet.private.enterprise (1.3.6.1.4.1)
				// followed by a '.' and an enterprise number (integer).
				if !strings.HasPrefix(e1.Val, "1.3.6.1.4.1.") {
					return fmt.Errorf("expected IANA registered SMI Netword Management Private Enterprise Code, got %s", e1.Val)
				}
				// Expect: [1-9][0-9]*(\.[1-9][0-9])*
				l := lexer{
					input: e1.Val[:len("1.3.6.1.4.1.")],
					line:  1,
					state: lexEnterpriseID,
				}
				for t := l.nextToken(); t.typ != tokenEOF; t = l.nextToken() {
					if t.typ == tokenError {
						return errors.New(t.val)
					}
				}

				// If we got here we verified that only integers separated by '.' were scanned.
				return nil
			default:
				return fmt.Errorf("expected IANA specified structured data parameter key 'ip' or 'enterpriseId', got %s", e1.Val)
			}
		}
	}

	// RFC5424 section 7.3.
	if meta, ok := sr.Data["meta"]; ok {
		if len(meta.Params) == 0 {
			return errors.New("optional IANA defined structured data id 'meta' defined, but no key-value pair parameters provided")
		}
		for _, e1 := range meta.Params {
			switch e1.Key {
			case "sequenceId":
				// seq must be in range [1, 2^31].
				seq, err := strconv.ParseUint(e1.Val, 10, 64)
				if err != nil || seq < 1 || seq > 2147483647 {
					return fmt.Errorf("expected sequence id in range [1, 2147483647], got %s", e1.Val)
				}
			case "sysUpTime":
				// integer representing the timeticks, in hundredths of a second, since the
				// exporter process last re-initialised (not the same as re-booted).
				ticks, err := strconv.ParseUint(e1.Val, 10, 64)
				if err != nil {
					return fmt.Errorf("expected system uptime in timeticks/integer, got %s", e1.Val)
				}

				now := time.Since(time.Unix(0, 0)).Milliseconds() / 10
				if ticks > uint64(now) {
					return errors.New("reported system uptime is longer than timeticks since January 1st 1970")
				}
			case "language":
				// Parse and verify RFC4646 language tags.
				l := lexer{
					input: e1.Val,
					line:  1,
					end:   len(e1.Val),
					state: lexLanguage,
				}
				if err := parseLanguage(&l); err != nil {
					return err
				}
			default:
				return fmt.Errorf("expected IANA specified structured data parameter key 'sequenceId', 'sysUpTime' or 'language', got %s", e1.Val)
			}
		}
	}
	return nil
}

// parseRFC5424 accepts a token stream and parses an RFC5424 syslog message from the token stream.
// An error is returned if the token stream does not constitute an RFC5424 message.
func parseRFC5424(l *lexer, sr *SyslogRecord) error {
	// Timestamp.
	t := l.nextToken()
	switch t.typ {
	case tokenTimestamp:
		ts, err := time.Parse(time.RFC3339Nano, t.val)
		if err != nil {
			return err
		}
		sr.Timestamp = ts
	case tokenNil:
		// Do nothing.
		sr.Timestamp = time.Now()
	case tokenError:
		return errors.New(t.val)
	default:
		return fmt.Errorf("expected %s, got %s", tDesc[tokenTimestamp], tDesc[t.typ])
	}

	// Hostname.
	t = l.nextToken()
	switch t.typ {
	case tokenNil:
		// Do nothing.
		break
	case tokenHostname:
		sr.Host = t.val
	default:
		return fmt.Errorf("expected %s or %s, got %s", tDesc[tokenHostname], tDesc[tokenNil], tDesc[t.typ])
	}

	// App name.
	t = l.nextToken()
	switch t.typ {
	case tokenNil:
		// Do nothing.
	case tokenAppname:
		sr.AppName = t.val
	default:
		return fmt.Errorf("expected %s or %s, got %s", tDesc[tokenAppname], tDesc[tokenNil], tDesc[t.typ])
	}

	// Process Id.
	t = l.nextToken()
	switch t.typ {
	case tokenNil:
		// Do nothing.
		break
	case tokenProcid:
		sr.ProcessId = t.val
	default:
		return fmt.Errorf("expected %s or %s, got %s", tDesc[tokenProcid], tDesc[tokenNil], tDesc[t.typ])
	}

	// Message ID.
	t = l.nextToken()
	switch t.typ {
	case tokenNil:
		// Do nothing.
		break
	case tokenMsgid:
		sr.MessageId = t.val
	default:
		return fmt.Errorf("expected %s or %s, got %s", tDesc[tokenMsgid], tDesc[tokenNil], tDesc[t.typ])
	}

	// Structured data.
	t = l.nextToken()
	switch t.typ {
	case tokenNil:
		// Do nothing.
		break
	case tokenBegin:
		sr.Data = make(map[string]StructuredData, 2)
		for ; t.typ == tokenBegin; t = l.nextToken() {
			sd := StructuredData{
				Params: make([]StructuredDataParam, 0, 2),
			}
			if t = l.nextToken(); t.typ != tokenSdId {
				return fmt.Errorf("expected %s, got %s", tDesc[tokenSdId], tDesc[t.typ])
			}

			if _, ok := sr.Data[t.val]; ok {
				return fmt.Errorf(
					"line %d:%d: duplicate structured data element ID, %q already defined",
					t.line,
					t.pos,
					t.val,
				)
			}
			sd.Id = t.val

			// Iterate Key-value pairs.
			for t = l.nextToken(); t.typ != tokenEnd; t = l.nextToken() {
				sdp := StructuredDataParam{}
				if t.typ == tokenKey {
					sdp.Key = t.val
				} else {
					return fmt.Errorf("line %d:%d: expected %s, got %s", t.line, t.pos, tDesc[tokenKey], tDesc[t.typ])
				}
				if t = l.nextToken(); t.typ == tokenVal {
					sdp.Val = t.val
					sd.Params = append(sd.Params, sdp)
				} else {
					return fmt.Errorf("line %d:%d: expected %s, got %s", t.line, t.pos, tDesc[tokenVal], tDesc[t.typ])
				}
			}

			// Append structured data element.
			sr.Data[sd.Id] = sd
		}
	default:
		return fmt.Errorf("line %d:%d: expected %s or %s, got %s", t.line, t.pos, tDesc[tokenBegin], tDesc[tokenNil], tDesc[t.typ])
	}

	// Message.
	if sr.Data == nil {
		t = l.nextToken()
	}
	switch t.typ {
	case tokenNil:
		// Do nothing.
		break
	case tokenMessage:
		sr.Message = t.val
	case tokenEOF:
		return nil
	default:
		return fmt.Errorf("expected %s or %s, got %s", tDesc[tokenAppname], tDesc[tokenNil], tDesc[t.typ])
	}

	// EOF.
	t = l.nextToken()
	if t.typ != tokenEOF && t.typ != tokenOctetCount {
		return fmt.Errorf("expected %s or %s, got %s", tDesc[tokenEOF], tDesc[tokenOctetCount], tDesc[t.typ])
	}
	return nil
}

// parseTimestamp3164 parses a stream of tokens into an RFC3164 timestamp, with
// optional sub-second precision and year. If the token stream isn't a timestamp
// the parser proceeds to parse the token stream as an RFC3164 hostname.
func parseTimestamp3164(t token, l *lexer, s *SyslogRecord) error {
	stamp := t.val
	nsec := time.Duration(0 * time.Nanosecond)
	if t.typ == tokenTimestampSubSecond {
		// Split the sub-second precision, because Go's stupid
		// time.StampNano must be exactly 9 decimals, not
		// variable.
		idx := strings.IndexByte(t.val, '.')
		stamp = t.val[:idx]
		nsec = time.Duration(unsafeAtoi(t.val[idx+1:]))
	}
	ts, err := time.Parse(time.Stamp, stamp)
	if err != nil {
		return fmt.Errorf("line %d:%d: invalid RFC3164 timestamp: %s, error message: %s", t.line, t.pos, t.val, err)
	}
	s.Timestamp = ts

	// Next token could be year, or hostname.
	year := 0
	t = l.nextToken()
	if t.typ == tokenInteger {
		y := unsafeAtoi(t.val)
		if y < 1 {
			return fmt.Errorf("line %d:%d: invalid RFC3164 timestamp year: %s", t.line, t.pos, t.val)
		}
		year = y
		t = l.nextToken()
	}

	// Construct the timestamp from parsed timestamp components.
	s.Timestamp = s.Timestamp.AddDate(year, 0, 0)
	s.Timestamp = s.Timestamp.Add(nsec)
	return parseHostname3164(t, l, s)
}

// parseHostname3164 expects the next token t to be a tokenHostname token.
func parseHostname3164(t token, l *lexer, s *SyslogRecord) error {
	switch t.typ {
	case tokenHostname:
		s.Host = t.val
		t = l.nextToken()
		return parseAppName3164(t, l, s)
	case tokenMessage:
		return parseMessage3164(t, l, s)
	case tokenEOF:
		return nil
	case tokenError:
		return errors.New(t.val)
	default:
		return fmt.Errorf(
			"expected RFC3164 syslog hostname, syslog message content or end of file, got %s",
			t.val,
		)
	}
}

// parseAppName3164 expects the next token t to be a tokenAppname token.
func parseAppName3164(t token, l *lexer, s *SyslogRecord) error {
	switch t.typ {
	case tokenAppname:
		s.AppName = t.val
		t = l.nextToken()
		return parseProcessId3164(t, l, s)
	case tokenMessage:
		return parseMessage3164(t, l, s)
	case tokenEOF:
		return nil
	case tokenError:
		return errors.New(t.val)
	default:
		return fmt.Errorf(
			"expected RFC3164 TAG, message or end of file, got %s",
			t.val,
		)
	}
}

// parseProcessId3164 expects the next token t to be a tokenProcid token.
func parseProcessId3164(t token, l *lexer, s *SyslogRecord) error {
	switch t.typ {
	case tokenProcid:
		s.ProcessId = t.val
		t = l.nextToken()
		return parseMessage3164(t, l, s)
	case tokenMessage:
		return parseMessage3164(t, l, s)
	case tokenEOF:
		return nil
	case tokenError:
		return errors.New(t.val)
	default:
		return fmt.Errorf(
			"expected RFC3164 process ID, message or end of file, got %s",
			t.val,
		)
	}
}

// parseMessage3164 expects the next token t to be a tokenMessage token.
func parseMessage3164(t token, l *lexer, s *SyslogRecord) error {
	switch t.typ {
	case tokenMessage:
		s.Message = t.val
		t = l.nextToken()
		return parseEOF3164(t)
	case tokenEOF:
		return nil
	case tokenError:
		return errors.New(t.val)
	default:
		return fmt.Errorf(
			"expected RFC3164 syslog message content, end of file or RFC6587 octet count, got %s",
			t.val,
		)
	}
}

// parseEOF3164 expects the next token t to be a tokenEOF token.
func parseEOF3164(t token) error {
	if t.typ == tokenEOF || t.typ == tokenOctetCount {
		return nil
	}
	if t.typ == tokenError {
		return errors.New(t.val)
	}
	return fmt.Errorf("expected end of file or RFC6587 octet count, got %s", t.val)
}

// parseLanguage parses the RFC5424 section 7.3.3 language tags per RFC4646.
// An error is returned if the the provided SD-ID value isn't an RFC4646
// language tag.
func parseLanguage(l *lexer) error {
	// Expect alpha of 2-3, 1-3, 4 or 5-8 characters OR privateuse.
	t := l.nextToken()
	if t.typ == tokenEOF {
		return errors.New("expected RFC4646 language-tag, got empty string")
	}
	if t.typ == tokenError {
		return errors.New(t.val)
	}
	if t.typ != tokenAlpha {
		return fmt.Errorf("line %d:%d: expected language tag, privateuse tag or grandfathered tag, got %s", t.line, t.pos, t.val)
	}

	if len(t.val) == 1 {
		if t.val[0] == 'x' || t.val[0] == 'X' {
			// Privateuse.
			return parsePrivateUse(l, t)
		}
		return parseGrandfathered(l)
	}
	if len(t.val) > 8 {
		return fmt.Errorf("line %d:%d: expected language tag of length [1, 8], got %s", t.line, t.pos, t.val)
	}
	if len(t.val) > 3 || len(t.val) < 1 {
		return fmt.Errorf("expected alphabetic sub-tag of length [1, 3], got %s", t.val)
	}
	// Could be RFC4646 language sub-tag or grandfathered.
	t = l.nextToken()
	if err := parseExtlang(l, t); err != nil {
		// Was not a language sub-tag. Try parsing as grandfathered.
		l.start = 0
		l.pos = 0
		l.state = lexLanguage
		return parseGrandfathered(l)
	}
	return nil
}

// ##################################
// ##### RFC4646 sub-tag states #####
// ##################################

// parseScript parses lexer l's token stream for an RFC4646 script tag.
// On success, the parser continues to parse the optional extension and/or
// privateuse tags. On failure, an error is returned.
func parseExtlang(l *lexer, t token) error {
	if t.typ == tokenEOF {
		return nil
	}
	for i1 := 0; t.typ != tokenEOF && i1 < 3; i1++ {
		if t.typ == '-' {
			t = l.nextToken()
			if t.typ == tokenEOF {
				return fmt.Errorf("line %d:%d: unexpected end of input", t.line, t.pos)
			}
		}
		if t.typ == tokenError {
			return errors.New(t.val)
		}
		if t.typ != tokenAlpha || len(t.val) != 3 {
			break
		}
		t = l.nextToken()
	}
	return parseScript(l, t)
}

// parseScript parses lexer l's token stream for an RFC4646 script tag.
// On success, the parser continues to parse the optional extension and/or
// privateuse tags. On failure, an error is returned.
func parseScript(l *lexer, t token) error {
	if t.typ == tokenEOF {
		return nil
	}
	if t.typ == '-' {
		t = l.nextToken()
		if t.typ == tokenEOF {
			return fmt.Errorf("line %d:%d: unexpected end of input", t.line, t.pos)
		}
	}
	if t.typ == tokenError {
		return errors.New(t.val)
	}
	if t.typ == tokenAlpha && len(t.val) == 4 && t.val[0] >= 'A' && t.val[0] <= 'Z' {
		// Doesn't fully verify ISO 15924 language codes, but verifies that
		// code being with capital letter and is of length 4.
		t = l.nextToken()
	}
	return parseRegion(l, t)
}

// parseRegion parses lexer l's token stream for an RFC4646 region tag.
// On success, the parser continues to parse the optional extension and/or
// privateuse tags. On failure, an error is returned.
func parseRegion(l *lexer, t token) error {
	if t.typ == tokenEOF {
		return nil
	}
	if t.typ == '-' {
		t = l.nextToken()
		if t.typ == tokenEOF {
			return fmt.Errorf("line %d:%d: unexpected end of input", t.line, t.pos)
		}
	}
	if t.typ == tokenError {
		return errors.New(t.val)
	}
	if t.typ == tokenInteger && len(t.val) == 3 {
		// UN M.49 code. Generaly integers in range [0, 999] are "valid".
		t = l.nextToken()
	} else if t.typ == tokenAlpha || t.typ == tokenAlnum && len(t.val) == 2 {
		// ISO 3166 code.
		t = l.nextToken()
	}
	return parseVariant(l, t)
}

// parseVariant parses lexer l's token stream for an RFC4646 variant tag.
// On success, the parser continues to parse the optional extension and/or
// privateuse tags. On failure, an error is returned.
func parseVariant(l *lexer, t token) error {
	if t.typ == tokenEOF {
		return nil
	}
	for t.typ != tokenEOF {
		// Accept 0 to unbounded variant sub-tags.
		if t.typ == '-' {
			t = l.nextToken()
			if t.typ == tokenEOF {
				return fmt.Errorf("line %d:%d: unexpected end of input", t.line, t.pos)
			}
		}
		if t.typ == tokenError {
			return errors.New(t.val)
		}
		if t.typ != tokenInteger && t.typ != tokenAlpha && t.typ != tokenAlnum {
			return fmt.Errorf("line %d:%d: expected RFC4646 variant, extension or privateuse, got %s", t.line, t.pos, t.val)
		}

		if len(t.val) == 1 && t.typ == tokenAlpha {
			// Is either RFC4646 extension or privateuse.
			if t.val[0] == 'x' || t.val[0] == 'X' {
				// Privateuse.
				return parsePrivateUse(l, t)
			}
			return parseExtension(l, t)
		}

		if len(t.val) < 4 || len(t.val) > 8 || ((t.val[0] < '0' || t.val[0] > '9') && len(t.val) == 4) {
			return parseExtension(l, t)
		}
		t = l.nextToken()
	}
	return parseExtension(l, t)
}

// parseExtenstion parses lexer l's token stream for an RFC4646 extension tag.
// If the input doesn't represent an extension tag, error is returned.
// On success, <nil> is returned.
func parseExtension(l *lexer, t token) error {
	if t.typ == tokenEOF {
		return nil
	}
	for t.typ != tokenEOF {
		if t.typ == '-' {
			t = l.nextToken()
			if t.typ == tokenEOF {
				return fmt.Errorf("line %d:%d: unexpected end of input", t.line, t.pos)
			}
		}
		if t.typ == tokenError {
			return errors.New(t.val)
		}
		if t.typ == tokenAlpha && len(t.val) == 1 && (t.val[0] == 'x' || t.val[0] == 'X') {
			return parsePrivateUse(l, t)
		}
		if t.typ != tokenInteger && t.typ != tokenAlpha && t.typ != tokenAlnum || (len(t.val) < 1 || len(t.val) > 8) {
			return fmt.Errorf("line %d:%d: expected RFC4646 extension, got %s", t.line, t.pos, t.val)
		}
		t = l.nextToken()
	}
	return parsePrivateUse(l, t)
}

// parsePrivateUse parses lexer l's token stream for an RFC4646 privateuse tag.
// If the input doesn't represent a privateuse tag, error is returned.
// On success, <nil> is returned.
func parsePrivateUse(l *lexer, t token) error {
	// Should have scanned 'x' or 'X' if parser got here.
	if t.typ == tokenEOF {
		return nil
	}
	if t.typ == tokenError {
		return errors.New(t.val)
	}
	if t.typ != tokenAlpha || (t.val[0] != 'x' && t.val[0] != 'X') {
		return fmt.Errorf("line %d:%d: expected 'x' or 'X', got %s", t.line, t.pos, t.val)
	}
	t = l.nextToken()
	i1 := 0
	for ; t.typ != tokenEOF; i1++ {
		if t.typ != '-' {
			return fmt.Errorf("line %d:%d: expected '-', got %s", t.line, t.pos, t.val)
		}
		t = l.nextToken()
		if t.typ == tokenEOF {
			return fmt.Errorf("line %d:%d: unexpected end of input", t.line, t.pos)
		}
		if t.typ != tokenAlnum && t.typ != tokenAlpha || len(t.val) < 1 || len(t.val) > 8 {
			return fmt.Errorf("line %d:%d: expected alphanumeric value of length [1, 8], got %s", t.line, t.pos, t.val)
		}
		t = l.nextToken()
	}

	if i1 < 1 {
		return fmt.Errorf("line %d:%d: expected at least 1 RFC4646 privateuse sub-tag", t.line, t.pos)
	}
	return nil
}

// parseGrandfathered parses lexer l's token stream for an RFC4646 grandfathered tag.
// If the input doesn't represent a grandfathered tag, error is returned.
// On success, <nil> is returned.
func parseGrandfathered(l *lexer) error {
	// Got 1*3ALPHA if parser got here.
	// Grandfathered is the last tag of the Language-Tag.
	i1 := 0
	t := l.nextToken()
	for ; t.typ != tokenEOF; t = l.nextToken() {
		if t.typ == tokenError {
			return errors.New(t.val)
		}
		if t.typ != '-' {
			fmt.Println("GRANDFATHERED")
			return fmt.Errorf("line %d:%d: expected '-', got %s", t.line, t.pos, t.val)
		}
		t = l.nextToken()
		if t.typ == tokenEOF {
			return fmt.Errorf("line %d:%d: unexpected end of input", t.line, t.pos)
		}
		if t.typ != tokenAlpha && t.typ != tokenAlnum && len(t.val) < 2 && len(t.val) > 8 {
			return fmt.Errorf("line %d:%d: expected alphanumeric value of length [2, 8], got %s", t.line, t.pos, t.val)
		}
		i1++
	}

	if i1 < 1 || i1 > 2 {
		return fmt.Errorf("expected 1 or 2 grandfathered sub-tags, got %d", i1)
	}

	// grandfathered is always the last tag in the RFC4646 syntax definition,
	// if present. Expect end of input.
	if t.typ != tokenEOF {
		return fmt.Errorf("line %d:%d: expected end of input after grandfathered element, got %s", t.line, t.pos, t.val)
	}
	return nil
}

// unsafeAtoi is like strconv.Atoi, but it doesn't check if the
// input string is an integer representation.
func unsafeAtoi(s string) int {
	signed := false
	if s[0] == '-' {
		signed = true
		s = s[1:]
	}
	i := 0
	for _, e1 := range s {
		i = (i << 3) + (i << 1) + (int(e1) - '0')
	}

	if signed {
		return -i
	}
	return i
}
