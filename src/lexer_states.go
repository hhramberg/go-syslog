// lexer_states.go holds the state functions of the lexer. A state function defines a state. Different states scans
// and tokenizes input differently.

package src

import (
	"strconv"
	"strings"
)

// ----------------------------
// ----- Type definitions -----
// ----------------------------

// ---------------------
// ----- Constants -----
// ---------------------

const (
	tokenEOF                tokenType = iota // Signals that end of input was reached.
	tokenError                               // Signals an error occurred during lexing.
	tokenNil                                 // The nil value for RFC5424.
	tokenPri                                 // Priority value of message.
	tokenVersion                             // Syslog version.
	tokenTimestamp                           // Timestamp of message.
	tokenTimestampSubSecond                  // Timestamp of message with sub-second precision.
	tokenMessage                             // The message part of the syslog message.
	tokenHostname                            // RFC5424 hostname.
	tokenAppname                             // RFC5424 application name.
	tokenProcid                              // RFC5424 process Id.
	tokenMsgid                               // RFC5424 message Id.
	tokenSdId                                // Structured data element Id.
	tokenKey                                 // Key of structured data Key-value pair.
	tokenVal                                 // Value of structured data Key-value pair.
	tokenBegin                               // Begin of structured data element.
	tokenEnd                                 // End of structured data element.
	tokenOctetCount                          // Octet count, number of bytes in one particular message.
	tokenInteger                             // tokenInteger defines a lexeme of an integer without leading zeroes.
	tokenAlpha                               // tokenAlpha defines a lexeme of a continous stream of ASCII English alphabeth characters.
	tokenAlnum                               // tokenAlpha defines a lexeme of a continous stream of ASCII English alphabeth characters and/or digits.
	tokenString                              // tokenString defines a lexeme of a string literal enclosed by string delimiters.
)

const (
	maxHostname3164 = 63  // Maximum length of RFC3164 hostname field, per RFC1034, section 3.1.
	maxHostname     = 255 // Maximum length of RFC5424 hostname field.
	maxAppname      = 48  // Maximum length of RFC5424 app name field.
	maxProcessId    = 128 // Maximum length of RFC5424 process id field.
	maxMessageId    = 32  // Maximum length of RFC5424 message id.
)

// --------------------
// ----- Globals ------
// --------------------

// tTyp provides string literals for tokenTypes.
var tTyp = [...]string{
	"tokenEOF",
	"tokenError",
	"tokenNil",
	"tokenPri",
	"tokenVersion",
	"tokenTimestamp",
	"tokenTimestampSubSecond",
	"tokenMessage",
	"tokenHostname",
	"tokenAppname",
	"tokenProcid",
	"tokenMsgid",
	"tokenSdId",
	"tokenKey",
	"tokenVal",
	"tokenBegin",
	"tokenEnd",
	"tokenOctetCount",
	"tokenInteger",
	"tokenAlpha",
	"tokenAlnum",
	"tokenString",
	"tokenVendorCiscoFacility",
	"tokenVendorCiscoSubFacility",
	"tokenVendorCiscoSeverity",
	"tokenVendorCiscoMnemonic",
}

// ----------------------
// ----- Functions ------
// ----------------------

// --------------------
// ----- Helpers ------
// --------------------

// isSdCharacter is the same as isPrintUsAscii, but it doesn't accept ' ', '=', '"' or ']'.
func isSdCharacter(r rune) bool {
	return (r >= 33 && r <= 126) && (r != '=' && r != ' ' && r != ']' && r != 34)
}

// isAlpha returns true if the rune r is an ASCII alphabetic character.
func isAlpha(c byte) bool {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
}

// isDigit returns true if rune r is a digit.
func isDigit(c byte) bool {
	return c >= '0' && c <= '9'
}

// isPrintUsAscii return true if rune r is ASCII character defined in RFC5424.
func isPrintUsAscii(c byte) bool {
	return c >= 33 && c <= 126
}

// isWhitespace returns true if byte c is a whitespace character as defined by Google RE2.
func isWhitespace(c byte) bool {
	return c == '\n' || c == ' ' || c == '\t' || c == '\f' || c == '\r'
}

// -------------------
// ----- States ------
// -------------------

// lexOctetCount scans the beginning of the input string for octet count per RFC
func lexOctetCount(l *lexer) token {
	c := l.next()
	if c >= '1' && c <= '9' {
		// Octet counting. Cannot be RFC5424 version, because PRI is mandatory in RFC5424.
		for c = l.next(); isDigit(c); c = l.next() {
		}

		if c != ' ' {
			return l.errorf("line: %d:%d: expected whitespace after octet count, got %c", l.line, l.linePos, c)
		}
		l.backup()

		t := l.emit(tokenOctetCount)
		l.next()
		l.ignore()

		if i, err := strconv.Atoi(t.val); err != nil {
			return l.errorf("%s is not a valid integer", t.val)
		} else {
			if i > len(l.input[l.start:]) {
				return l.errorf("octet count suggests message is %d bytes long, "+
					"but message is only %d bytes long", i-len(t.val)-1, len(l.input[l.start:]))
			}
			l.end = l.start + i
		}
		l.state = lexPri
		return t
	}
	l.backup()
	return lexPri(l)
}

// lexPri scans the string for a priority value per RFC5424.
func lexPri(l *lexer) token {
	c := l.next()

	if c == '<' {
		// Priority value is not required per RFC3164, but is highly recommended.
		l.ignore()

		for c = l.next(); isDigit(c); c = l.next() {
		}
		if c != '>' {
			return l.errorf("line %d:%d: expected '>', got %c", l.line, l.linePos, c)
		}
		l.backup()
		tok := l.emit(tokenPri)
		l.next()
		l.ignore()
		l.state = lexVersion
		return tok
	}
	l.backup()
	l.state = lexTimestampRFC3164
	return l.state(l)
}

// lexVersion scans the string for syslog version number per RFC5424.
func lexVersion(l *lexer) token {
	c := l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}
	if c != ' ' {
		// Is not RFC5424 version. Must be RFC3164.
		l.reset()
		l.state = lexTimestampRFC3164
		return l.state(l)
	}
	l.backup()
	if l.pos > l.start {
		l.state = lexTimestampRFC5424
		return l.emit(tokenVersion)
	}
	l.state = lexTimestampRFC3164
	return l.state(l)
}

// lexTimestampRFC5424 scans the input for an RFC3339 timestamp.
func lexTimestampRFC5424(l *lexer) token {
	c := l.next()
	// Ignore leading whitespace.
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}

	// Check for nil value.
	if c == '-' {
		l.state = lexHostname
		return l.emit(tokenNil)
	}

	// Scan YYYY.
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 4; i1++ {
		c = l.next()
	}
	if c != '-' {
		return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
	}

	// Scan MM.
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}
	if c != '-' {
		return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
	}

	// Scan DD.
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}

	if c != 'T' {
		return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
	}

	// Scan hh.
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}
	if c != ':' {
		return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
	}

	// Scan mm.
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}
	if c != ':' {
		return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
	}

	// Scan ss.
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}

	// Check for milliseconds.
	if c == '.' {
		if !l.accept("0123456789") {
			return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
		}
		c = l.next()
		for i1 := 0; isDigit(c) && i1 < 5; i1++ {
			c = l.next()
		}
	}

	// Offset.
	if c == 'Z' {
		l.state = lexHostname
		return l.emit(tokenTimestamp)
	}
	if c != '-' && c != '+' {
		return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
	}
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}
	if c != ':' {
		return l.errorf("%q is not a valid RFC3339 timestamp", l.input[l.start:l.pos])
	}
	c = l.next()
	for i1 := 0; isDigit(c) && i1 < 2; i1++ {
		c = l.next()
	}
	l.backup()
	l.state = lexHostname
	return l.emit(tokenTimestamp)
}

// lexHostname scans the input for RFC5424 hostname.
func lexHostname(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}

	l.state = lexAppname

	if c == '-' {
		return l.emit(tokenNil)
	} else {
		for i1 := 0; isPrintUsAscii(c) && i1 < maxHostname; i1++ {
			c = l.next()
		}
		if c != ' ' {
			return l.errorf("expected whitespace, got %c", c)
		}
		l.backup()
		return l.emit(tokenHostname)
	}
}

// lexHostname3164 scans the input for RFC3164 hostname, IPv4 or IPv6 address.
func lexHostname3164(l *lexer) token {
	c := l.next()
	// Ignore leading whitespace.
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}
	if c == eof {
		return l.emit(tokenEOF)
	}

	for n := 0; isPrintUsAscii(c) && n < maxHostname3164; n++ {
		// RFC1034: domain label is at most 63 characters long.
		c = l.next()
	}
	if c != ' ' && c != eof {
		return l.errorf("expected whitespace or end of file, got %c", c)
	}

	l.backup()
	l.state = lexTag
	return l.emit(tokenHostname)
}

// lexAppname scans the input for RFC5424 application name.
func lexAppname(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}
	l.state = lexProcessId5424

	if c == '-' {
		return l.emit(tokenNil)
	} else {
		for i1 := 0; isPrintUsAscii(c) && i1 < maxAppname; i1++ {
			c = l.next()
		}
		if c != ' ' {
			return l.errorf("expected whitespace, got %c", c)
		}
		l.backup()
		return l.emit(tokenAppname)
	}
}

// lexProcessId5424 scans the input for RFC5424 process id.
func lexProcessId5424(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}
	l.state = lexMessageId

	if c == '-' {
		return l.emit(tokenNil)
	} else {
		for i1 := 0; isPrintUsAscii(c) && i1 < maxProcessId; i1++ {
			c = l.next()
		}
		if c != ' ' {
			return l.errorf("expected whitespace, got %c", c)
		}
		l.backup()
		return l.emit(tokenProcid)
	}
}

// lexMessageId scans the input for RFC5424 message id.
func lexMessageId(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}
	l.state = lexStructuredData

	if c == '-' {
		return l.emit(tokenNil)
	} else {
		for i1 := 0; isPrintUsAscii(c) && i1 < maxMessageId; i1++ {
			c = l.next()
		}
		if c != ' ' {
			return l.errorf("expected whitespace, got %c", c)
		}
		l.backup()
		return l.emit(tokenMsgid)
	}
}

// lexTag scans the input for RFC3164 message TAG field, if it exists, per RFC3164 section 4.1.3.
func lexTag(l *lexer) token {
	c := l.next()
	for isWhitespace(c) {
		l.ignore()
		c = l.next()
	}

	for i1 := 0; (isAlpha(c) || c == '_' || c == '-') && i1 < maxAppname; i1++ {
		c = l.next()
	}

	switch c {
	case '[':
		l.state = lexProcessId3164
		l.backup()
		return l.emit(tokenAppname)
	case ':':
		l.backup()
		t := l.emit(tokenAppname)
		l.next()
		l.ignore()
		l.state = lexMessage
		return t
	default:
		return lexMessage(l)
	}
}

// lexProcessId3164 scans the input for a process id (PID) per RFC3164, section 4.1.3.
func lexProcessId3164(l *lexer) token {
	// We know c == '[' if lexer got here.
	l.next()
	l.ignore()

	c := l.next()
	for i1 := 0; isDigit(c) && i1 < maxProcessId; i1++ {
		c = l.next()
	}

	if c != ']' {
		l.reset()
		return lexMessage(l)
	}
	c = l.next()
	if c != ':' {
		l.reset()
		return lexMessage(l)
	}

	// Don't include trailing "]:".
	l.backup()
	l.backup()

	t := l.emit(tokenProcid)

	l.next()
	l.next()
	l.ignore()
	l.ignore()

	l.state = lexMessage
	return t
}

// lexStructuredData scans the input for structured data per RFC5424.
func lexStructuredData(l *lexer) token {
	c := l.next()
	// Ignore leading whitespace.
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}

	if c == '-' {
		l.state = lexMessage
		return l.emit(tokenNil)
	}

	if c != '[' {
		return lexMessage(l)
	}
	l.state = lexStructuredDataId
	return l.emit(tokenBegin)

}

// lexStructuredDataId scans the input string for structured data element id.
func lexStructuredDataId(l *lexer) token {
	// Lex SD-ID.
	c := l.next()
	for i1 := 0; i1 < 32 && isSdCharacter(rune(c)); i1++ {
		c = l.next()
	}
	l.backup()

	l.state = lexStructuredDataKey
	return l.emit(tokenSdId)
}

// lexStructuredDataKey scans the input string for structured data parameter key.
func lexStructuredDataKey(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c); c = l.next() {
	}

	if c == ']' {
		l.state = lexStructuredData
		return l.emit(tokenEnd)
	}
	l.backup()
	l.ignore()

	c = l.next()
	for i1 := 0; i1 < 32; i1++ {
		if c == '=' {
			break
		}
		if c < 33 || c > 126 || c == '"' || c == ']' {
			return l.errorf("character %c not allowed in structured data parameter key", c)
		}
		c = l.next()
	}
	l.backup()
	tok := l.emit(tokenKey)

	c = l.next()
	if c != '=' {
		return l.errorf("expected =, got %c", c)
	}
	l.ignore()

	l.state = lexStructuredDataValue
	return tok
}

// lexStructuredDataValue scans the input string for structured data value.
func lexStructuredDataValue(l *lexer) token {
	c := l.next()
	if c != '"' {
		return l.errorf("expected \" at beginning of structured data value, got %c", c)
	}
	l.ignore()

	r := l.nextUTF8() // Previous character for checking escaped characters.
	if r == eof {
		return l.errorf("unexpected end of file")
	}
	if r == ']' {
		return l.errorf("unescaped %c", r)
	}
	if r != '"' {
		for rr := l.nextUTF8(); ; rr = l.nextUTF8() {
			if rr == eof {
				return l.errorf("unexpected end of file")
			}
			if r != '\\' {
				if rr == '"' {
					break
				}
				if rr == ']' {
					return l.errorf("unescaped %c", rr)
				}
				r = rr
			} else {
				// Cancel the escape sequence by setting previous to some safe value.
				r = 0
			}
		}
	}

	l.backup()
	tok := l.emit(tokenVal)
	l.next()
	l.ignore()
	l.state = lexStructuredDataKey
	return tok
}

// lexMessage scans the remaining input as a single UTF-8 compatible string.
func lexMessage(l *lexer) token {
	// Ignore leading whitespace.
	for c := l.next(); isWhitespace(c); c = l.next() {
		l.ignore()
	}
	if len(l.input[l.start:]) > 0 {
		l.pos = l.end
		if len(l.input[l.start:l.pos]) == 1 && l.input[l.start] == '-' {
			return l.emit(tokenNil)
		}
		t := l.emit(tokenMessage)
		if len(l.input[l.pos:]) > 0 {
			// Octet counting. Read new syslog message from input string.
			l.start = l.pos
			l.linePos = 0
			l.state = lexOctetCount
		} else {
			l.state = lexEof
		}
		return t
	}

	// Tell parser that we reached end of input.
	return lexEof(l)
}

// lexEof returns an end-of-file token.
func lexEof(l *lexer) token {
	if c := l.next(); c != eof {
		return l.errorf("expected end of file, got %c", c)
	}
	return l.emit(tokenEOF)
}

// lexTimestampRFC3164 scans lexer l's input for components of an RFC3164
// timestamp, with optional sub-second precision.
func lexTimestampRFC3164(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}

	// Months: 	Jan, Feb, Mar, Apr, May, Jun
	//			Jul, Aug, Sep, Oct, Nov, Des
	//
	// Jan
	// Jun
	// Jul
	// Aug
	// Apr
	// Mar
	// May
	// Des
	// Feb
	// Nov
	// Oct
	// Sep

	// Scan month.
	if strings.IndexByte("JAMDFNOS", c) < 0 {
		l.backup()
		return lexHostname3164(l)
	}

	// Let parser decide whether months is OK or not.
	i1 := 0
	for c = l.next(); c >= 'a' && c <= 'y' && i1 < 2; c = l.next() {
		i1++
	}
	if i1 != 2 && !isWhitespace(c) {
		l.reset()
		return lexHostname3164(l)
	}
	c = l.next()
	if isWhitespace(c) {
		c = l.next()
	}

	// Scan day of month.
	if !isDigit(c) {
		l.reset()
		return lexHostname3164(l)
	}
	for c = l.next(); isDigit(c); c = l.next() {
	}
	if !isWhitespace(c) {
		l.reset()
		return lexHostname3164(l)
	}

	// Scan timestamp HH:mm:ss.
	for c = l.next(); isDigit(c); c = l.next() {
	}
	if c != ':' {
		l.reset()
		return lexHostname3164(l)
	}
	for c = l.next(); isDigit(c); c = l.next() {
	}
	if c != ':' {
		l.reset()
		return lexHostname3164(l)
	}
	for c = l.next(); isDigit(c); c = l.next() {
	}

	// Scan optional sub-second precision.
	var t token
	if c == '.' {
		c = l.next()
		if !isDigit(c) {
			l.reset()
			return lexHostname3164(l)
		}
		for c = l.next(); isDigit(c); c = l.next() {
		}
		l.backup()
		t = l.emit(tokenTimestampSubSecond)
	} else {
		l.backup()
		t = l.emit(tokenTimestamp)
	}
	l.state = lexTimestampYearRFC3164
	return t
}

// lexTimestampYear3164 scans lexer l's input for an optional year integer
// that immediately follows the RFC3164 timestamp.
func lexTimestampYearRFC3164(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c); c = l.next() {
		l.ignore()
	}

	if isDigit(c) {
		for c = l.next(); isDigit(c); c = l.next() {
		}
		if !isWhitespace(c) && c != eof {
			// Indicates hostname.
			l.reset()
			return lexHostname3164(l)
		}
		l.backup()
		l.state = lexHostname3164
		return l.emit(tokenInteger)
	}
	l.backup()
	return lexHostname3164(l)
}

// ####################################
// ##### RFC5424 section 7 states #####
// ####################################

// lexEnterpriseID scans lexer l's input string for an SMI for a private
// enterprise ID. This state function is used during the optional verification
// pass of RFC5424 IANA specified structured data elements, per RFC5424 section
// 7.2.2.
func lexEnterpriseID(l *lexer) token {
	c := l.next()
	switch {
	case c == '0':
		// Can't accept leading zeroes.
		c := l.next()
		if c != '.' && c != eof {
			return l.errorf("expected integer, got %s", l.input[l.start:l.pos])
		}
		t := l.emit(tokenInteger)
		l.next()
		l.ignore()
		return t
	case isDigit(c):
		for ; isDigit(c); c = l.next() {
		}
		if c != '.' && c != eof {
			return l.errorf("expected integer, got %s", l.input[l.start:l.pos])
		}
		l.backup()
		t := l.emit(tokenInteger)
		l.next()
		l.ignore()
		return t
	case c == eof:
		// Alert caller that end of input was succesfully reached.
		return token{
			typ: eof,
			pos: len(l.input),
		}
	default:
		return l.errorf("expected integer, got %s", l.input[l.start:l.pos])
	}
}

// lexInteger scans lexer l's input for an integer.
func lexInteger(l *lexer) token {
	c := l.next()
	if c == '0' {
		if isDigit(l.peek()) {
			return l.errorf("integers can't have leading zeroes")
		}
		return l.emit(tokenInteger)
	}
	for ; isDigit(c); c = l.next() {
	}

	if c != eof {
		l.backup()
	}
	return l.emit(tokenInteger)
}

// lexLanguage scans lexer l's input for an RFC5424 section 7.3.3 language
// tag as defined by RFC4646.
func lexLanguage(l *lexer) token {
	for c := l.next(); ; c = l.next() {
		switch {
		case isAlpha(c):
			for ; isAlpha(c); c = l.next() {
			}
			if c != eof {
				l.backup()
			}
			if isDigit(c) {
				return lexAlnum(l)
			}
			return l.emit(tokenAlpha)
		case isDigit(c):
			for ; isDigit(c); c = l.next() {
			}
			if c != eof {
				l.backup()
			}
			if isAlpha(c) {
				return lexAlnum(l)
			}
			return l.emit(tokenAlpha)
		case c == '-':
			return l.emit('-')
		case c == eof:
			return l.emit(tokenEOF)
		default:
			return l.errorf("expected alphabetical character, digit or '-', got %c", c)
		}
	}
}

// lexAlnum scans lexer l's input for an alphanumeric character stream.
func lexAlnum(l *lexer) token {
	c := l.next()
	for ; isDigit(c) || isAlpha(c); c = l.next() {
	}
	l.backup()
	return l.emit(tokenAlnum)
}

// #################################
// ##### Key-value pair states #####
// #################################

// lexKey scans lexer l's input for a key-value pair key of type
// key:value or key=value.
func lexKey(l *lexer) token {
	c := l.next()
	for ; isWhitespace(c) || c == '_' || c == '-'; c = l.next() {
		// Key cannot begin with '_' or '-'. Also ignore leading whitespace.
		l.ignore()
	}
	if c == eof {
		l.state = nil
		return l.emit(tokenEOF)
	}
	if c == '"' || c == '\'' {
		t := lexString(l, c)
		if t.typ == tokenError {
			return t
		}
		t.typ = tokenKey
		c = l.next()
		l.ignore()
		if c == ':' || c == '=' {
			l.state = lexValue
			t.typ = tokenKey
			return t
		}
		// If lexer got, the string literal was not a key.
	}

	i1 := 0

	// Accept a run of English alphabet ASCII characters, '-' and '_' delimited by
	// a ':' or '='.
	for ; isAlpha(c) || isDigit(c) || c == '-' || c == '_'; c = l.next() {
		i1++
	}
	if i1 < 1 {
		l.ignore()
		return lexKey(l)
	}
	if l.input[l.pos-1] == '_' || l.input[l.pos-1] == '-' {
		// Can't end on '_' or '-'.
		l.ignore()
		return lexKey(l)
	}
	if c != ':' && c != '=' {
		l.ignore()
		return lexKey(l)
	}
	l.backup()
	t := l.emit(tokenKey)
	l.next()
	l.ignore()
	l.state = lexValue
	return t
}

// lexValue scans lexer l's input for a key-value pair value of type
// key:value or key=value.
func lexValue(l *lexer) token {
	c := l.next()
	if c == eof {
		l.state = nil
		return l.emit(tokenEOF)
	}
	if isWhitespace(c) || c == '_' || c == '-' {
		// Value must follow ':' or '=' immediately, and cannot
		// begin with '_' or '-'.
		l.ignore()
		return lexKey(l)
	}

	if c == '"' || c == '\'' {
		t := lexString(l, c)
		t.typ = tokenVal
		return t
	}

	i1 := 0

	// Accept a run of English alphabet ASCII characters, '-' and '_' delimited by
	// a whitespace or end of input.
	for ; isAlpha(c) || isDigit(c) || c == '-' || c == '_'; c = l.next() {
		i1++
	}
	l.backup()
	if i1 < 1 {
		l.ignore()
		return lexKey(l)
	}
	if l.input[l.pos-1] == '_' || l.input[l.pos-1] == '-' {
		// Can't end on '_' or '-'.
		l.ignore()
		return lexKey(l)
	}
	l.state = lexKey
	return l.emit(tokenVal)
}

// lexString scans lexer l's input for a string literal enclosed by sep
// as enclosing tags. Does not supprot escaped '"' or '\”. Returns an error
// if end of input is reached before the closing '"' or '\”.
func lexString(l *lexer, sep byte) token {
	l.ignore() // Ignore enclosing '"' and '\''.
	for c := l.next(); c != sep; c = l.next() {
		if c == eof {
			return l.errorf("unexpected end of input in string literal")
		}
	}
	l.backup()
	t := l.emit(tokenString)
	l.next()
	l.ignore()
	return t
}

// ##################################
// ##### Vendor decoding states #####
// ##################################

// lexVendorCiscoFacility scans the input string for a Cisco style syslog message facility identifier.
func lexVendorCiscoFacility(l *lexer) token {
	c := l.next()
	if c != '%' {
		return l.errorf("not a Cisco syslog message")
	}
	l.ignore()
	for c = l.next(); c != eof && c != '-' && c != ':'; c = l.next() {
	}
	if c == eof {
		return l.errorf("unexpected end of file")
	}
	l.backup()
	t := l.emit(tokenVendorCiscoFacility)
	l.next()
	if c == '-' {
		l.state = lexVendorCiscoSubFacility
	} else {
		l.state = nil
	}
	l.ignore()
	return t
}

// lexVendorCiscoSubFacility scans the input string for a Cisco style sub-facility identifier. If the first
// character is a number, the input is determined to be severity.
func lexVendorCiscoSubFacility(l *lexer) token {
	c := l.next()
	if c >= '0' && c <= '9' {
		l.backup()
		return lexVendorCiscoSeverity(l)
	}
	for ; c != eof && c != '-' && c != ':'; c = l.next() {
	}
	if c == eof {
		return l.errorf("unexpected end of file")
	}
	l.backup()
	t := l.emit(tokenVendorCiscoSubFacility)
	l.next()
	if c == '-' {
		l.state = lexVendorCiscoSeverity
	} else {
		l.state = nil
	}
	l.ignore()
	return t
}

// lexVendorCiscoSeverity scans the input string for a Cisco style syslog message severity identifier.
func lexVendorCiscoSeverity(l *lexer) token {
	c := l.next()
	for ; c != eof && c != '-' && c != ':'; c = l.next() {
	}
	if c == eof {
		return l.errorf("unexpected end of file")
	}
	l.backup()
	t := l.emit(tokenVendorCiscoSeverity)
	l.next()
	if c == '-' {
		l.state = lexVendorCiscoMnemonic
	} else {
		l.state = nil
	}
	l.ignore()
	return t
}

// lexVendorCiscoMnemonic scans the input string for a Cisco style syslog message mnemonic.
func lexVendorCiscoMnemonic(l *lexer) token {
	c := l.next()
	for ; c != eof && c != ':'; c = l.next() {
	}
	if c == eof {
		return l.errorf("unexpected end of file")
	}
	l.backup()
	tok := l.emit(tokenVendorCiscoMnemonic)
	l.next()
	if l.next() != ' ' {
		return l.errorf("expected ': ' after mnemonic, got %q", l.input[l.start:l.pos])
	}
	l.ignore()
	l.state = nil
	return tok
}

// lexCEF scans lexer l's input, from the current position, for the constant
// Common Event Format (CEF) prefix "CEF:0|", which is the prefix for any CEF message.
//
// Currently only CEF version 0 (dated to 2010 by ArcSoft) is supported.
func lexCEF(l *lexer) token {
	c := l.next()
	if c != 'C' {
		goto cefHeaderError
	}
	c = l.next()
	if c != 'E' {
		goto cefHeaderError
	}
	c = l.next()
	if c != 'F' {
		goto cefHeaderError
	}
	c = l.next()
	if c != ':' {
		goto cefHeaderError
	}
	l.ignore()
	c = l.next()
	if c != '0' {
		return l.errorf("expected CEF version 0, got '%s'", l.input[l.start:l.pos])
	}
	l.state = lexCEFPrefixString
	return l.emit(tokenVersion)
cefHeaderError:
	return l.errorf("expected CEF version, got '%s'", l.input[l.start:l.pos])
}

// lexCEFPrefixString scans lexer l's input from the current position for an arbitrary string
// terminated by a '|' (pipe) character. The emitted token will be of type tokenAlnum.
func lexCEFPrefixString(l *lexer) token {
	c := l.nextUTF8()
	if c == eof {
		return l.errorf("unexpeced end of input")
	}
	if c != '|' {
		return l.errorf("expected '|', got %c", c)
	}
	l.ignore()
	for c = l.nextUTF8(); c != eof && c != '|'; c = l.nextUTF8() {
	}
	l.backup()
	return l.emit(tokenAlnum)
}

// lexCEFKey scans lexer l's input from the current position for a Common Event Format (CEF)
// key-value pair key.
func lexCEFKey(l *lexer) token {
	c := l.nextUTF8()
	if c == eof {
		return token{typ: tokenEOF}
	}
	if c != '|' {
		return l.errorf("expected '|', got %c", c)
	}
	l.ignore()

	for ; c != eof && c != ' ' && c != '\t' && c != '='; c = l.nextUTF8() {

	}
	if c == eof {
		return l.errorf("unexpected end of file")
	}

	l.backup()
	t := l.emit(tokenKey)
	l.next()
	l.ignore()
	l.state = lexCEFValue
	return t
}

// lexCEFValue scans lexer l's input from the current position for a Common Event Format (CEF)
// key-value pair value.
func lexCEFValue(l *lexer) token {
	c := l.nextUTF8()
	prev := rune(0)

	// Break if unescaped '=' is found.
	for ; c != eof && !(c == '=' && prev != '\\'); c = l.nextUTF8() {
		prev = c
	}
	l.backup()
	t := l.emit(tokenVal)
	l.next()
	l.ignore()
	l.state = lexCEFKey
	return t
}
