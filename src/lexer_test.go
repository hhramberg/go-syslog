// lexer_test.go provides unit tests to verify that the lexer correctly scans and tokenizes RFC5424 and RFC3164
// syslog messages.

package src

import (
	"testing"
)

// ----------------------------
// ----- Type definitions -----
// ----------------------------

// lexTest wraps a byte sequence and its expected token sequence.
type lexTest struct {
	in  string
	out []token
}

// lexBench encapsulates a benchmark input and name.
type lexBench struct {
	name  string    // The informative name of the benchmark.
	in    string    // The input string to feed the lexer for the benchmark.
	state stateFunc // The state of the lexer for the benchmark.
}

// ---------------------
// ----- Constants -----
// ---------------------

// --------------------
// ----- Globals ------
// --------------------

// lexTests3164 defines a series of lexTest data stream and token stream pairs for RFC3164 compliant messages.
var lexTests3164 = [...]lexTest{
	{
		in: "<34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8",
		out: []token{
			// Message: <34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8
			{
				typ: tokenPri,
				val: "34",
			},
			{
				typ: tokenTimestamp,
				val: "Oct 11 22:14:15",
			},
			{
				typ: tokenHostname,
				val: "mymachine",
			},
			{
				typ: tokenAppname,
				val: "su",
			},
			{
				typ: tokenMessage,
				val: "'su root' failed for lonvick on /dev/pts/8",
			},
		},
	},
	{
		in: "Use the BFG!",
		out: []token{
			// Message: Use the BFG!
			{
				typ: tokenHostname,
				val: "Use",
			},
			{
				typ: tokenMessage,
				val: "the BFG!",
			},
		},
	},
	{
		in: "<13>Feb  5 17:32:18 10.0.0.99 Use the BFG!",
		out: []token{
			// Message: <13>Feb 5 17:32:18 10.0.0.99 Use the BFG!
			{
				typ: tokenPri,
				val: "13",
			},
			{
				typ: tokenTimestamp,
				val: "Feb  5 17:32:18",
			},
			{
				typ: tokenHostname,
				val: "10.0.0.99",
			},
			{
				typ: tokenMessage,
				val: "Use the BFG!",
			},
		},
	},
	{
		in: "<165>Aug 24 05:34:00 CST 1987 mymachine myproc[10]: %% It's time to make the do-nuts.  %%  Ingredients: Mix=OK, Jelly=OK # Devices: Mixer=OK, Jelly_Injector=OK, Frier=OK # Transport: Conveyer1=OK, Conveyer2=OK # %%",
		out: []token{
			// Message: <165>Aug 24 05:34:00 CST 1987 mymachine myproc[10]: %% It's time to make the do-nuts.  %%  Ingredients: Mix=OK, Jelly=OK # Devices: Mixer=OK, Jelly_Injector=OK, Frier=OK # Transport: Conveyer1=OK, Conveyer2=OK # %%
			{
				typ: tokenPri,
				val: "165",
			},
			{
				typ: tokenTimestamp,
				val: "Aug 24 05:34:00",
			},
			{
				typ: tokenHostname,
				val: "CST",
			},
			{
				typ: tokenMessage,
				val: "1987 mymachine myproc[10]: %% It's time to make the do-nuts.  %%  Ingredients: Mix=OK, Jelly=OK # Devices: Mixer=OK, Jelly_Injector=OK, Frier=OK # Transport: Conveyer1=OK, Conveyer2=OK # %%",
			},
		},
	},
	{
		// This is a particularly difficult example, as stated in RFC3164 page 16. Because the current version of
		// the lexer strictly follows the RFC3164 specification it doesn't allow alternate timestamps than
		// RFC3164 or RFC3339. The 1990 is too large to be syslog version (per RFC5424), so it instead becomes
		// the hostname.
		in: "<0>1990 Oct 22 10:52:01 TZ-6 scapegoat.dmz.example.org 10.1.2.3 sched[0]: That's All Folks!",
		out: []token{
			// Message: <0>1990 Oct 22 10:52:01 TZ-6 scapegoat.dmz.example.org 10.1.2.3 sched[0]: That's All Folks!
			{
				typ: tokenPri,
				val: "0",
			},
			{
				typ: tokenHostname,
				val: "1990",
			},
			{
				typ: tokenMessage,
				val: "Oct 22 10:52:01 TZ-6 scapegoat.dmz.example.org 10.1.2.3 sched[0]: That's All Folks!",
			},
		},
	},
	{
		// This is a particularly difficult example, as stated in RFC3164 page 16. Because the current version of
		// the lexer strictly follows the RFC3164 specification it doesn't allow alternate timestamps than
		// RFC3164 or RFC3339. The 1990 is too large to be syslog version (per RFC5424), so it instead becomes
		// the hostname.
		in: "<0>Oct 22 10:52:01 scapegoat.dmz.example.org sched[3156]: That's All Folks!",
		out: []token{
			// Message: <0>1990 Oct 22 10:52:01 TZ-6 scapegoat.dmz.example.org 10.1.2.3 sched[0]: That's All Folks!
			{
				typ: tokenPri,
				val: "0",
			},
			{
				typ: tokenTimestamp,
				val: "Oct 22 10:52:01",
			},
			{
				typ: tokenHostname,
				val: "scapegoat.dmz.example.org",
			},
			{
				typ: tokenAppname,
				val: "sched",
			},
			{
				typ: tokenProcid,
				val: "3156",
			},
			{
				typ: tokenMessage,
				val: "That's All Folks!",
			},
		},
	},
	{
		in: "<165>Oct 11 12:34:56 db02-datastore hard disks are nearly full, usage is 90%",
		out: []token{
			// Message: "<165>Oct 11 12:34:56 db02-datastore hard disk are nearly full, usage is 90%"
			{
				typ: tokenPri,
				val: "165",
			},
			{
				typ: tokenTimestamp,
				val: "Oct 11 12:34:56",
			},
			{
				typ: tokenHostname,
				val: "db02-datastore",
			},
			{
				typ: tokenMessage,
				val: "hard disks are nearly full, usage is 90%",
			},
		},
	},
}

// lexTests5424 defines a series of lexTest data stream and token stream pairs for RFC5424 compliant messages.
var lexTests5424 = [...]lexTest{
	{
		in: "<34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - 'su root' failed for lonvick on /dev/pts/8",
		out: []token{
			// Message: <34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - 'su root' failed for lonvick on
			// /dev/pts/8
			{
				typ: tokenPri,
				val: "34",
			},
			{
				typ: tokenVersion,
				val: "1",
			},
			{
				typ: tokenTimestamp,
				val: "2003-10-11T22:14:15.003Z",
			},
			{
				typ: tokenHostname,
				val: "mymachine.example.com",
			},
			{
				typ: tokenAppname,
				val: "su",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMsgid,
				val: "ID47",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMessage,
				val: "'su root' failed for lonvick on /dev/pts/8",
			},
		},
	},
	{
		in: "<165>1 2003-08-24T05:14:15.000003-07:00 192.0.2.1 myproc 8710 - - %% It's time to make the do-nuts.",
		out: []token{
			// Message: <165>1 2003-08-24T05:14:15.000003-07:00 192.0.2.1 myproc 8710 - - %% It's time to make the do-nuts.
			{
				typ: tokenPri,
				val: "165",
			},
			{
				typ: tokenVersion,
				val: "1",
			},
			{
				typ: tokenTimestamp,
				val: "2003-08-24T05:14:15.000003-07:00",
			},
			{
				typ: tokenHostname,
				val: "192.0.2.1",
			},
			{
				typ: tokenAppname,
				val: "myproc",
			},
			{
				typ: tokenProcid,
				val: "8710",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMessage,
				val: "%% It's time to make the do-nuts.",
			},
		},
	},
	{
		in: "<34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - 'su root' failed for lonvick on /dev/pts/8",
		out: []token{
			// Message: <165>1 2003-08-24T05:14:15.000003-07:00 192.0.2.1 myproc 8710 - - %% It's time to make the do-nuts.
			{
				typ: tokenPri,
				val: "34",
			},
			{
				typ: tokenVersion,
				val: "1",
			},
			{
				typ: tokenTimestamp,
				val: "2003-10-11T22:14:15.003Z",
			},
			{
				typ: tokenHostname,
				val: "mymachine.example.com",
			},
			{
				typ: tokenAppname,
				val: "su",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMsgid,
				val: "ID47",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMessage,
				val: "'su root' failed for lonvick on /dev/pts/8",
			},
		},
	},
	{
		in: "<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut=\"3\" eventSource=\"Application\" eventID=\"1011\"] BOMAn application event log entry...",
		out: []token{
			// Message: <165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut="3"
			// eventSource="Application" eventID="1011"] BOMAn application event log entry...
			{
				typ: tokenPri,
				val: "165",
			},
			{
				typ: tokenVersion,
				val: "1",
			},
			{
				typ: tokenTimestamp,
				val: "2003-10-11T22:14:15.003Z",
			},
			{
				typ: tokenHostname,
				val: "mymachine.example.com",
			},
			{
				typ: tokenAppname,
				val: "evntslog",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMsgid,
				val: "ID47",
			},
			{
				typ: tokenBegin,
				val: "[",
			},
			{
				typ: tokenSdId,
				val: "exampleSDID@32473",
			},
			{
				typ: tokenKey,
				val: "iut",
			},
			{
				typ: tokenVal,
				val: "3",
			},
			{
				typ: tokenKey,
				val: "eventSource",
			},
			{
				typ: tokenVal,
				val: "Application",
			},
			{
				typ: tokenKey,
				val: "eventID",
			},
			{
				typ: tokenVal,
				val: "1011",
			},
			{
				typ: tokenEnd,
				val: "]",
			},
			{
				typ: tokenMessage,
				val: "BOMAn application event log entry...",
			},
		},
	},
	{
		in: "<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut=\"3\" eventSource=\"Application\" eventID=\"1011\"][examplePriority@32473 class=\"high\"]",
		out: []token{
			// Message: <165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut="3"
			// eventSource="Application" eventID="1011"][examplePriority@32473 class="high"]
			{
				typ: tokenPri,
				val: "165",
			},
			{
				typ: tokenVersion,
				val: "1",
			},
			{
				typ: tokenTimestamp,
				val: "2003-10-11T22:14:15.003Z",
			},
			{
				typ: tokenHostname,
				val: "mymachine.example.com",
			},
			{
				typ: tokenAppname,
				val: "evntslog",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMsgid,
				val: "ID47",
			},
			{
				typ: tokenBegin,
				val: "[",
			},
			{
				typ: tokenSdId,
				val: "exampleSDID@32473",
			},
			{
				typ: tokenKey,
				val: "iut",
			},
			{
				typ: tokenVal,
				val: "3",
			},
			{
				typ: tokenKey,
				val: "eventSource",
			},
			{
				typ: tokenVal,
				val: "Application",
			},
			{
				typ: tokenKey,
				val: "eventID",
			},
			{
				typ: tokenVal,
				val: "1011",
			},
			{
				typ: tokenEnd,
				val: "]",
			},
			{
				typ: tokenBegin,
				val: "[",
			},
			{
				typ: tokenSdId,
				val: "examplePriority@32473",
			},
			{
				typ: tokenKey,
				val: "class",
			},
			{
				typ: tokenVal,
				val: "high",
			},
			{
				typ: tokenEnd,
				val: "]",
			},
		},
	},
	{
		in: "56 <1>1 2003-10-11T22:14:15.003Z host - - - - Short message58 <1>1 2003-10-11T22:14:15.003Z host - - - - Shorter message",
		out: []token{
			// Message: <1>1 2003-10-11T22:14:15.003Z host - - - - Short message
			{
				typ: tokenOctetCount,
				val: "56",
			},
			{
				typ: tokenPri,
				val: "1",
			},
			{
				typ: tokenVersion,
				val: "1",
			},
			{
				typ: tokenTimestamp,
				val: "2003-10-11T22:14:15.003Z",
			},
			{
				typ: tokenHostname,
				val: "host",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMessage,
				val: "Short message",
			},
			{
				typ: tokenOctetCount,
				val: "58",
			},
			{
				typ: tokenPri,
				val: "1",
			},
			{
				typ: tokenVersion,
				val: "1",
			},
			{
				typ: tokenTimestamp,
				val: "2003-10-11T22:14:15.003Z",
			},
			{
				typ: tokenHostname,
				val: "host",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenNil,
				val: "-",
			},
			{
				typ: tokenMessage,
				val: "Shorter message",
			},
		},
	},
}

// lexBench3164 defines a series of lexBench data for benchmarking lexing of multiple types of RFC3164 syslog messages.
var lexBench3164 = [...]lexBench{
	{
		name:  "MinimalPri",
		in:    "<1>",
		state: lexPri,
	},
	{
		name:  "MinimalPriZero",
		in:    "<0>",
		state: lexPri,
	},
	{
		name:  "MinimalPriMax",
		in:    "<165>",
		state: lexPri,
	},
	{
		name:  "TimestampMin",
		in:    "Feb  5 17:32:18",
		state: lexTimestampRFC3164,
	},
	{
		name:  "TimestampMax",
		in:    "Oct 11 22:14:15",
		state: lexTimestampRFC3164,
	},
}

// lexBench3164 defines a series of lexBench data for benchmarking lexing of multiple types of RFC5424 syslog messages.
var lexBench5424 = [...]lexBench{
	{
		name:  "TimestampMin",
		in:    "2003-10-11T22:14:15Z",
		state: lexTimestampRFC5424,
	},
	{
		name:  "TimestampMax",
		in:    "2003-10-11T22:14:15.003211-01:30",
		state: lexTimestampRFC5424,
	},
	{
		name:  "SDKey",
		in:    "example-param-name-thats-sd-name=",
		state: lexStructuredDataKey,
	},
	{
		name: "SDKeyLong",
		in: "\"loooooongestest very long super not short parameter value, muuuuuch " +
			"longer than the previous longester parameter values, really looooooong\"",
		state: lexStructuredDataValue,
	},
	{
		name: "SDKeyLongEscaped",
		in: "\"loooooongestest \"very\"\\] \"long\" \"super\\]\\]\\] not \"short\" \"parameter\"\\\"\\\\ \\ value, " +
			"muuuuuch \\\"longer than the previous longester parameter values, really looooooong\"",
		state: lexStructuredDataValue,
	},
}

// ----------------------
// ----- Functions ------
// ----------------------

// ------------------
// ----- Tests ------
// ------------------

// TestLexer3164 verifies that the serial lexer scans and tokenizes RFC5424 compatible syslog messages correctly.
func TestLexer3164(t *testing.T) {
	for i1, e1 := range lexTests3164 {
		// RFC3164.
		l := lexer{
			input: e1.in,
			line:  1,
			end:   len(e1.in),
			state: lexOctetCount,
		}

		i2 := 0
		for tok := l.nextToken(); tok.typ != tokenEOF; tok = l.nextToken() {
			if i2 >= len(e1.out) {
				t.Errorf("test case %d: expected %d tokens, got %d", i1+1, len(e1.out), i2+1)
				break
			}
			exp := e1.out[i2]
			if i2 >= len(e1.out) {
				t.Errorf("test case %d: expected %d tokens, got %d", i1+1, len(e1.out), i2+1)
				break
			}
			if exp.typ != tokenError {
				if tok.typ != exp.typ || tok.val != exp.val {
					t.Errorf("test case %d: expected %s, got %s", i1+1, exp.String(), tok.String())
				}
			} // If error is expected: don't do anything.
			if tok.typ == tokenError && exp.typ != tokenError {
				t.Errorf("test case %d: expected %s, got %s", i1+1, exp.String(), tok.String())
				break
			}
			i2++
		}
	}
}

// TestLexer5424 verifies that the serial lexer scans and tokenizes RFC5424 compatible syslog messages correctly.
func TestLexer5424(t *testing.T) {
	for i1, e1 := range lexTests5424 {
		// RFC5424.
		l := lexer{
			input: e1.in,
			line:  1,
			end:   len(e1.in),
			state: lexOctetCount,
		}

		i2 := 0
		for tok := l.nextToken(); tok.typ != tokenEOF; tok = l.nextToken() {
			if i2 >= len(e1.out) {
				t.Errorf("test case %d: expected %d tokens, got %d", i1+1, len(e1.out), i2+1)
				break
			}
			exp := e1.out[i2]
			if exp.typ != tokenError {
				if tok.typ != exp.typ || tok.val != exp.val {
					t.Errorf("test case %d: expected %s, got %s", i1+1, exp.String(), tok.String())
				}
			} // If error is expected: don't do anything.
			if tok.typ == tokenError && exp.typ != tokenError {
				t.Errorf("test case %d: expected %s, got %s", i1+1, exp.String(), tok.String())
				break
			}
			i2++
		}
	}
}

// TestLexer5424StructuredDataValueEscaped verifies that the lexer scans and tokenizes RFC5424 structured data
// values correctly when they contain multiple escaped characters such as '\', '"' and ']'.
func TestLexer5424StructuredDataValueEscaped(t *testing.T) {
	//const end byte = 93     // Character ]
	//const quote byte = 34   // Character "
	//const slash byte = 92   // Character \

	// testWrap wraps test data and expected output token type.
	type testWrap struct {
		in  string    // Input string to scan.
		out tokenType // Expected token type.
		val string    // Expected output.
	}

	tests := []testWrap{
		// 1. The empty value.
		{
			in:  "\"\"",
			out: tokenVal,
			val: "",
		},
		// 2. Almost empty value.
		{
			in:  "\" \"",
			out: tokenVal,
			val: " ",
		},
		// 3. Single unescaped \, expect error.
		{
			in:  "\"\\\"",
			out: tokenError,
			val: "",
		},
		// 4. Single unescaped ", message is malformed but conforms to RFC5424.
		{
			in:  "\"\"\"",
			out: tokenVal,
			val: "",
		},
		// 5. Single unescaped ], expect error.
		{
			in:  "\"]\"",
			out: tokenError,
			val: "",
		},
		// 6. Single escaped \.
		{
			in:  "\"\\\\\"",
			out: tokenVal,
			val: "\\\\",
		},
		// 7. Single escaped ".
		{
			in:  "\"\\\"\"",
			out: tokenVal,
			val: "\\\"",
		},
		// 8. Single escaped ].
		{
			in:  "\"\\]\"",
			out: tokenVal,
			val: "\\]",
		},
		// 9. Escaped " at the end, expect error.
		{
			in:  "\"\\]\\",
			out: tokenError,
			val: "",
		},
		// 10. Unescaped ], expect error
		{
			in:  "\" ]\"",
			out: tokenError,
			val: "",
		},
		// 11. Unclosed value, expect error
		{
			in:  "\" unclosed",
			out: tokenError,
			val: "",
		},
		// 12. Unclosed value with escape character, expect error
		{
			in:  "\" unclosed\\",
			out: tokenError,
			val: "",
		},
		// 13. No start quotation mark.
		{
			in:  "\\ \"",
			out: tokenError,
			val: "",
		},
		// 14. Unescaped value.
		{
			in:  "\"Lorem ipsum dolor sit amet.\"",
			out: tokenVal,
			val: "Lorem ipsum dolor sit amet.",
		},
		// 15. Properly escaped quotation mark at beginning.
		{
			in:  "\"\\\"Lorem ipsum dolor sit amet.\"",
			out: tokenVal,
			val: "\\\"Lorem ipsum dolor sit amet.",
		},
		// 16. Unclosed quoted value.
		{
			in:  "\"\\\"Lorem ipsum dolor sit amet.\\\"",
			out: tokenError,
			val: "",
		},
		// 17. Properly quoted value.
		{
			in:  "\"\\\"Lorem ipsum dolor sit amet.\\\"\"",
			out: tokenVal,
			val: "\\\"Lorem ipsum dolor sit amet.\\\"",
		},
	}

	for i1, e1 := range tests {
		l := lexer{line: 1}
		l.input = e1.in
		l.state = lexStructuredDataValue
		if tok := l.nextToken(); tok.typ != e1.out {
			if e1.out == tokenError {
				t.Errorf("Test entry %d: expected %s, got %s", i1+1, tTyp[e1.out], tTyp[tok.typ])
			} else {
				t.Errorf("Test entry %d: expected %s, got error: %s", i1+1, tTyp[e1.out], tok.val)
			}
		} else if tok.typ != tokenError && tok.val != e1.val {
			t.Errorf("Test entry %d: expected %s, got %s", i1+1, e1.in[1:len(e1.in)-1], tok.val)
		}
	}
}

// -----------------------
// ----- Benchmarks ------
// -----------------------

// BenchmarkLexer3164 benchmarks lexing RFC3164 compliant syslog messages.
func BenchmarkLexer3164(b *testing.B) {
	for _, e1 := range lexBench3164 {
		b.Run(e1.name, func(b *testing.B) {
			l := lexer{
				input: e1.in,
				state: e1.state,
			}
			for n := 0; n < b.N; n++ {
				l.nextToken()
				l.state = e1.state
				l.start = 0
				l.linePos = 0
				l.pos = 0
			}
		})
	}
}

// BenchmarkLexer5424 benchmarks lexing RFC5424 compliant syslog messages.
func BenchmarkLexer5424(b *testing.B) {
	for _, e1 := range lexBench5424 {
		b.Run(e1.name, func(b *testing.B) {
			l := lexer{
				input: e1.in,
				state: e1.state,
			}
			for n := 0; n < b.N; n++ {
				l.nextToken()
				l.state = e1.state
				l.start = 0
				l.linePos = 0
				l.pos = 0
			}
		})
	}
}

func BenchmarkLexer5424SDValue(b *testing.B) {
	in := "\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\""
	l := lexer{
		input: in,
		state: lexStructuredDataValue,
	}
	for n := 0; n < b.N; n++ {
		l.nextToken()
		l.state = lexStructuredDataValue
		l.start = 0
		l.pos = 0
	}
}
