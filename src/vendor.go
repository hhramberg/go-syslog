package src

import (
	"errors"
	"fmt"
	"strconv"
)

const (
	tokenVendorCiscoFacility = tokenString + 1 + iota
	tokenVendorCiscoSubFacility
	tokenVendorCiscoSeverity
	tokenVendorCiscoMnemonic
)

// DecodeCisco inspects the syslog message part for Cisco specific syslog facility and mnemonic. Severity is ignored;
// it has already been decoded in the original syslog header.
func DecodeCisco(s *SyslogRecord) {
	if s == nil {
		return
	}

	if _, ok := s.Data["vendor"]; ok {
		// Vendor information already parsed.
		return
	}

	l := lexer{
		input: s.Message,
		line:  1,
		state: lexVendorCiscoFacility,
	}

	// Expect '%' to initiate Cisco syslog message.
	vendor := StructuredData{
		Id: "vendor",
		Params: []StructuredDataParam{
			{
				Key: "name",
				Val: "Cisco",
			},
			{
				Key: "facility",
			},
			{
				Key: "mnemonic",
			},
		},
	}

	// Cisco facility.
	if t := l.nextToken(); t.typ == tokenError {
		return
	} else {
		vendor.Params[1].Val = t.val
	}

	// Cisco sub-facility.
	if t := l.nextToken(); t.typ == tokenError {
		return
	} else if t.typ == tokenVendorCiscoSubFacility {
		if t = l.nextToken(); t.typ != tokenVendorCiscoSeverity {
			return
		}
		// Also ignore severity.
	}

	// Mnemonic.
	if t := l.nextToken(); t.typ == tokenError {
		return
	} else {
		vendor.Params[2].Val = t.val
	}

	// Truncate message and append facility and mnemonic to structured data.
	s.Message = s.Message[l.start:]
	if s.Data == nil {
		s.Data = make(map[string]StructuredData, 1)
	}
	s.Data["vendor"] = vendor
}

// ParseKeyValuePairs scans SyslogRecord sr's Message for key-value pairs
// fo the form key:value or key=value. Keys and values must be English alphabet
// ASCII characters, including '_' and '-', not separated by whitespace characters.
// Key-value pairs are stored in SyslogRecord sr's Data under the "key-value" key.
func ParseKeyValuePairs(sr *SyslogRecord) error {
	l := lexer{
		input: sr.Message,
		line:  1,
		end:   len(sr.Message),
		state: lexKey,
	}
	if sr.Data == nil {
		sr.Data = make(map[string]StructuredData, 2)
	}
	pairs := StructuredData{
		Id:     "key-value",
		Params: make([]StructuredDataParam, 0, 4),
	}

	for t := l.nextToken(); t.typ != tokenEOF; {
		var key token

		// Expect key.
		switch t.typ {
		case tokenKey:
			key = t
		case tokenEOF:
			sr.Data["key-value"] = pairs
			return nil
		case tokenError:
			return errors.New(t.val)
		default:
			// Not a key-value pair.
			t = l.nextToken()
			continue
		}

		// Expect value.
		t = l.nextToken()
		switch t.typ {
		case tokenVal:
			pairs.Params = append(pairs.Params, StructuredDataParam{Key: key.val, Val: t.val})
			t = l.nextToken()
		case tokenKey:
			continue
		case tokenEOF:
			sr.Data["key-value"] = pairs
			return nil
		case tokenError:
			return errors.New(t.val)
		default:
			// Not a key-value pair.
			t = l.nextToken()
			continue
		}
	}

	sr.Data["key-value"] = pairs
	return nil
}

// ParseCEF scans SyslogRecord sr's Message for Common Event Format (CEF)
// header and key-value pairs. Key-value pairs are stored in SyslogRecord sr's
// Data under the "CEF" key.
//
// The CEF parser is comliant with the 2007 CEF version 0, as defined in this
// document from ArcSight: https://raffy.ch/blog/wp-content/uploads/2007/06/CEF.pdf
func ParseCEF(sr *SyslogRecord) error {
	// "CEF:0|Vendor|Product|Version|Signature|Event name|Severity|key-value pairs"
	// Severity must be in range [0, 10], where 10 is most severe.
	l := lexer{
		input: sr.Message,
		line:  1,
		end:   len(sr.Message),
		state: lexCEF,
	}
	if sr.Data == nil {
		sr.Data = make(map[string]StructuredData, 2)
	}
	cef := StructuredData{
		Id:     "cef",
		Params: make([]StructuredDataParam, 0, 8), // 8 is a reasonable number of pre-allocations; CEF prefix is 6 elements.
	}

	// Parse the CEF header/prefix. Expect version first.
	t := l.nextToken()
	switch t.typ {
	case tokenVersion:
		i, err := strconv.Atoi(t.val)
		if err != nil || i != 0 {
			return fmt.Errorf("line %d:%d: expected CEF version 0, got %s", t.line, t.pos, t.val)
		}
		cef.Params = append(cef.Params, StructuredDataParam{Key: "cef-version", Val: t.val})
	case tokenError:
		return errors.New(t.val)
	case tokenEOF:
		return fmt.Errorf("line %d:%d: unexpected end of file", t.line, t.pos)
	default:
		return fmt.Errorf("line %d:%d: expected CEF version 0, got %s", t.line, t.pos, t.val)
	}

	// Expect an arbitrary alphanumeric string for CEF vendor.
	t = l.nextToken()
	switch t.typ {
	case tokenAlnum:
		cef.Params = append(cef.Params, StructuredDataParam{Key: "vendor", Val: t.val})
	case tokenError:
		return errors.New(t.val)
	case tokenEOF:
		return fmt.Errorf("line %d:%d: unexpected end of file", t.line, t.pos)
	default:
		return fmt.Errorf("line %d:%d: expected CEF vendor, got %s", t.line, t.pos, t.val)
	}

	// Expect an arbitrary alphanumeric string for CEF product.
	t = l.nextToken()
	switch t.typ {
	case tokenAlnum:
		cef.Params = append(cef.Params, StructuredDataParam{Key: "product", Val: t.val})
	case tokenError:
		return errors.New(t.val)
	case tokenEOF:
		return fmt.Errorf("line %d:%d: unexpected end of file", t.line, t.pos)
	default:
		return fmt.Errorf("line %d:%d: expected CEF product, got %s", t.line, t.pos, t.val)
	}

	// Expect an arbitrary alphanumeric string for CEF product version.
	t = l.nextToken()
	switch t.typ {
	case tokenAlnum:
		cef.Params = append(cef.Params, StructuredDataParam{Key: "version", Val: t.val})
	case tokenError:
		return errors.New(t.val)
	case tokenEOF:
		return fmt.Errorf("line %d:%d: unexpected end of file", t.line, t.pos)
	default:
		return fmt.Errorf("line %d:%d: expected CEF product version, got %s", t.line, t.pos, t.val)
	}

	// Expect an arbitrary alphanumeric string for CEF signature.
	t = l.nextToken()
	switch t.typ {
	case tokenAlnum:
		cef.Params = append(cef.Params, StructuredDataParam{Key: "signature", Val: t.val})
	case tokenError:
		return errors.New(t.val)
	case tokenEOF:
		return fmt.Errorf("line %d:%d: unexpected end of file", t.line, t.pos)
	default:
		return fmt.Errorf("line %d:%d: expected CEF signature, got %s", t.line, t.pos, t.val)
	}

	// Expect an arbitrary alphanumeric string for CEF event name.
	t = l.nextToken()
	switch t.typ {
	case tokenAlnum:
		cef.Params = append(cef.Params, StructuredDataParam{Key: "event name", Val: t.val})
	case tokenError:
		return errors.New(t.val)
	case tokenEOF:
		return fmt.Errorf("line %d:%d: unexpected end of file", t.line, t.pos)
	default:
		return fmt.Errorf("line %d:%d: expected CEF event name, got %s", t.line, t.pos, t.val)
	}

	// Expect an arbitrary alphanumeric string for CEF severity.
	t = l.nextToken()
	switch t.typ {
	case tokenAlnum:
		i, err := strconv.Atoi(t.val)
		if err != nil || i < 0 || i > 10 {
			return fmt.Errorf("line %d:%d: expected CEF severity in range [0, 10], got %s", t.line, t.pos, t.val)
		}
		cef.Params = append(cef.Params, StructuredDataParam{Key: "severity", Val: t.val})
	case tokenError:
		return errors.New(t.val)
	case tokenEOF:
		return fmt.Errorf("line %d:%d: unexpected end of file", t.line, t.pos)
	default:
		return fmt.Errorf("line %d:%d: expected CEF severity, got %s", t.line, t.pos, t.val)
	}

	// Accept an arbitrary number of key-value pairs.
	l.state = lexCEFKey
	for t := l.nextToken(); t.typ != tokenEOF; t = l.nextToken() {
		switch t.typ {
		case tokenKey:
			key := t.val
			t = l.nextToken()
			if t.typ == tokenError {
				return errors.New(t.val)
			}
			if t.typ != tokenVal {
				return fmt.Errorf("line %d:%d: expected a CEF key-value pair value, got: %s", t.line, t.pos, t.val)
			}
			cef.Params = append(cef.Params, StructuredDataParam{Key: key, Val: t.val})
		case tokenError:
			return errors.New(t.val)
		default:
			return fmt.Errorf("unexpected CEF sequence at line %d:%d: %s", t.line, t.line, t.val)
		}
	}

	// Set, and possibly overwrite, StructuredData element with key/id "cef" of SyslogRecord sr.
	sr.Data[cef.Id] = cef
	return nil
}
