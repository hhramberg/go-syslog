// lexer.go provides a state machine inspired by Rob Pikes talk on lexical scanning in Go.
// The state machine traverses an input string and emits tokens. A token has a distinct
// type and a lexical value. See lexer_states.go for the lexer state functions.

package src

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

// ----------------------------
// ----- Type definitions -----
// ----------------------------

// stateFunc defines the state of the lexer.
type stateFunc func(l *lexer) token

// tokenType distinguishes tokens.
type tokenType int

// token carries both a distinct type specified by the lexer, a
// lexical value and where in the input string it was scanned.
type token struct {
	typ  tokenType // The type of the lexeme.
	val  string    // The value of the lexeme.
	line int       // Line in input stream lexeme begins.
	pos  int       // Position of line lexeme begins.
}

// lexer is a lexer that tokenizes an input string sequentially.
type lexer struct {
	input      string    // String to tokenize.
	start      int       // Current start of token in string.
	pos        int       // Current position in string.
	end        int       // Expected end of current syslog message.
	line       int       // Line in string of the current lexeme.
	linePos    int       // Position on current line the current lexeme begins.
	scanLength int       // Number of bytes scanned since the beginning of the current lexeme.
	width      int       // Size of most recently scanned character in bytes.
	state      stateFunc // State of lexer.
}

// ---------------------
// ----- Constants -----
// ---------------------

const eof = 0 // Same as \0 for zero-terminated C-strings.

// --------------------
// ----- Globals ------
// --------------------

// ----------------------
// ----- Functions ------
// ----------------------

// accept consumes the next ASCII character if it's from the valid set.
func (l *lexer) accept(s string) bool {
	if strings.IndexByte(s, l.next()) >= 0 {
		return true
	}
	l.backup()
	return false
}

// acceptRun consumes a run of ASCII characters from the valid set.
// func (l *lexer) acceptRun(s string) {
// 	for strings.IndexByte(s, l.next()) >= 0 {
// 	}
// 	l.backup()
// }

// backup steps back one scanned ASCII character.
func (l *lexer) backup() {
	l.pos -= l.width
}

// emit sends a token of type t to the caller over the token channel.
func (l *lexer) emit(t tokenType) token {
	res := token{
		typ:  t,
		val:  l.input[l.start:l.pos],
		line: l.line,
		pos:  l.linePos,
	}
	l.width = 0
	l.start = l.pos
	return res
}

// errorf alerts the parser of a scan error by returning an error token.
func (l *lexer) errorf(format string, args ...interface{}) token {
	l.state = nil
	return token{
		typ:  tokenError,
		val:  fmt.Sprintf("line %d:%d: %s", l.line, l.linePos, fmt.Sprintf(format, args...)),
		line: l.line,
		pos:  l.linePos,
	}
}

// ignore skips the currently scanned string.
func (l *lexer) ignore() {
	l.linePos += len(l.input[l.start:l.pos])
	l.start = l.pos
	l.width = 0
}

// next scans the next ASCII character in the input stream.
func (l *lexer) next() byte {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	c := l.input[l.pos]
	l.width = 1
	l.pos++
	l.linePos++
	return c
}

// next scans the next ASCII character in the input stream.
func (l *lexer) nextUTF8() (r rune) {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	r, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width
	l.linePos++
	l.scanLength += l.width
	return r
}

// nextToken returns the next token from the lexer.
func (l *lexer) nextToken() token {
	return l.state(l)
}

// peek returns the next rune, but does not advance the lexer's position.
func (l *lexer) peek() byte {
	c := l.next()
	l.backup()
	return c
}

// reset sets the current position of the lexer to the start position.
func (l *lexer) reset() {
	l.width = 0
	l.linePos -= l.scanLength
	l.scanLength = 0
	l.pos = l.start
}

// String returns a print friendly string of the token t.
func (t token) String() string {
	if t.typ > tokenVendorCiscoMnemonic {
		return t.val
	}
	switch t.typ {
	case tokenEOF:
		return "EOF"
	case tokenError:
		return t.val
	}
	if len(t.val) > 40 {
		return fmt.Sprintf("[%s] %.40q...", tTyp[t.typ], t.val)
	}
	return fmt.Sprintf("[%s] %q", tTyp[t.typ], t.val)
}

// Value is like String, but does not include the token type, only the scanned value.
func (t token) Value() string {
	switch t.typ {
	case tokenEOF:
		return "EOF"
	case tokenError:
		return t.val
	}
	if len(t.val) > 40 {
		return fmt.Sprintf("%.40q...", t.val)
	}
	return fmt.Sprintf("%q", t.val)
}

// Type is like String, but does not include the scanned value of the token, onle the token type.
func (t token) Type() string {
	if int(t.typ) >= len(tTyp) {
		return "### token type string not defined ###"
	}
	return fmt.Sprintf("[%s]", tTyp[t.typ])
}
