package src

import (
	"fmt"
	"strconv"
)

// ----------------------------
// ----- Type definitions -----
// ----------------------------

// ianaValidateFunc defines a function that takes a structured data element and validates the parameters of the
// element against IANA-registered SD elements per RFC5424 section 7.
type ianaValidateFunc func(sd StructuredData) error

// ianaWrapper wraps an IANA-registered SD-ID and a map of reserved key values.
type ianaWrapper struct {
	id     string   // The IANA-registered string SD-ID.
	params []string // Parameter keys defined for the IANA-registered SD-ID.
}

// ---------------------
// ----- Constants -----
// ---------------------

// --------------------
// ----- Globals ------
// --------------------

// reservedIds defines the IANA-registered SD-IDs.
var reservedIds = [...]ianaWrapper{
	{id: "timeQuality", params: []string{
		"tzKnown",
		"isSynced",
		"syncAccuracy",
	}},
	{id: "origin", params: []string{
		"ip",
		"enterpriseId",
		"software",
		"swVersion",
	}},
	{id: "meta", params: []string{
		"sequenceId",
		"sysUpTime",
		"language",
	}},
}

// lut defines a lookup table for quickly calling the correct validating function from VerifyIanaFields.
var lut = [...]ianaValidateFunc{
	verifyTimeQuality,
	verifyOrigin,
	verifyMeta,
}

// ----------------------
// ----- Functions ------
// ----------------------

// VerifyIanaFields checks the SyslogRecord's structured data for IANA-registered SD-IDs, per RFC5424, section 7.
// If such fields exists, and are valid, the function returns <nil>. If fields exists and there are errors,
// the function returns an error message, detailing the structured data element error. If no elements with
// IANA-registered SD-IDs exist, the function returns <nil>.
func (sr *SyslogRecord) VerifyIanaFields() error {
	for _, e1 := range sr.Data {
		for i1, e2 := range reservedIds {
			if e1.Id == e2.id {
				// Validate the key-value pairs of the SD element.
				if err := lut[i1](e1); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

// verifyTimeQuality verifies that the StructuredData element conforms to RFC5424 section 7.1.
// If it does not, an error is returned. If it conforms, <nil> is returned.
func verifyTimeQuality(sd StructuredData) error {
	isSynced, isSyncAccuracy := false, false
	for _, e1 := range sd.Params {
		switch e1.Key {
		case "tzKnown":
			if len(e1.Val) != 1 {
				return fmt.Errorf("IANA-registered SD-ID %q, parameter value length must be 1, got %d",
					sd.Id, len(e1.Val))
			}
			if e1.Val[0] != '0' && e1.Val[0] != '1' {
				return fmt.Errorf("IANA-registered SD-ID %q, parameter key %q must be either '0' or '1'",
					sd.Id, e1.Key)
			}
		case "isSynced":
			if len(e1.Val) != 1 {
				return fmt.Errorf("IANA-registered SD-ID %q, parameter value length must be 1, got %d",
					sd.Id, len(e1.Val))
			}
			if e1.Val[0] == '1' {
				isSynced = true
				continue
			}
			if e1.Val[0] != '0' {
				return fmt.Errorf("IANA-registered SD-ID %q, parameter key %q must be either '0' or '1'",
					sd.Id, e1.Key)
			}
		case "syncAccuracy":
			if _, err := strconv.Atoi(e1.Val); err != nil {
				return fmt.Errorf("IANA-registered SD-ID %q, parameter %q: %q is not a valid integer",
					sd.Id, e1.Key, e1.Val)
			}
			isSyncAccuracy = true
		default:
			return fmt.Errorf("IANA-registered SD-ID %q does not define parameter key %q",
				sd.Id, e1.Key)
		}
	}
	if isSyncAccuracy && !isSynced {
		return fmt.Errorf("IANA-registered SD-ID %q: got parameter \"syncAccuracy\", but parameter \"isSynced\" is either missing or set to '0'",
			sd.Id)
	}
	return nil
}

// verifyOrigin verifies that the StructuredData element conforms to RFC5424 section 7.2.
// If it does not, an error is returned. If it conforms, <nil> is returned.
func verifyOrigin(sd StructuredData) error {
	for _, e1 := range sd.Params {
		switch e1.Key {
		case "ip":
		case "enterpriseId":
		case "software":
		case "swVersion":
		default:
			return fmt.Errorf("IANA-registered SD-ID %q does not define parameter key %q",
				sd.Id, e1.Key)
		}
	}
	return nil
}

// verifyMeta verifies that the StructuredData element conforms to RFC5424 section 7.3.
// If it does not, an error is returned. If it conforms, <nil> is returned.
func verifyMeta(sd StructuredData) error {
	for _, e1 := range sd.Params {
		switch e1.Key {
		case "sequenceId":
		case "sysUpTime":
		case "language":
		default:
			return fmt.Errorf("IANA-registered SD-ID %q does not define parameter key %q",
				sd.Id, e1.Key)
		}
	}
	return nil
}
