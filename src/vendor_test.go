package src

import (
	"testing"
	"time"
)

// ----------------------------
// ----- Type definitions -----
// ----------------------------

// ---------------------
// ----- Constants -----
// ---------------------

// --------------------
// ----- Globals ------
// --------------------

// ciscoLexTestMessages defines unit tests for lexing a Cisco syslog message part.
var ciscoLexTestMessages = [...]lexTest{
	{
		in: "%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/0, changed state to down",
		out: []token{
			{
				typ:  tokenVendorCiscoFacility,
				val:  "LINEPROTO",
				line: 1,
				pos:  2,
			},
			{
				typ:  tokenVendorCiscoSeverity,
				val:  "5",
				line: 1,
				pos:  12,
			},
			{
				typ:  tokenVendorCiscoMnemonic,
				val:  "UPDOWN",
				line: 1,
				pos:  14,
			},
		},
	},
	{
		in: "%SYS-5-CONFIG_I: Configured from console by console",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYS",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "5",
			},
			{
				typ: tokenVendorCiscoMnemonic,
				val: "CONFIG_I",
			},
		},
	},
	{
		in: "%SYS-6-LOGGINGHOST_STARTSTOP: Logging to host 192.168.1.239 stopped - CLI initiated",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYS",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "6",
			},
			{
				typ: tokenVendorCiscoMnemonic,
				val: "LOGGINGHOST_STARTSTOP",
			},
		},
	},
	{
		in: "%SYSMGR-STANDBY-3-SHUTDOWN_START: The System Manager has started the shutdown procedure.",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYSMGR",
			},
			{
				typ: tokenVendorCiscoSubFacility,
				val: "STANDBY",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "3",
			},
			{
				typ: tokenVendorCiscoMnemonic,
				val: "SHUTDOWN_START",
			},
		},
	},
	{
		in: "%SYSMGR-STANDBY-21-SHUTDOWN_START: The System Manager has started the shutdown procedure.",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYSMGR",
			},
			{
				typ: tokenVendorCiscoSubFacility,
				val: "STANDBY",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "21",
			},
			{
				typ: tokenVendorCiscoMnemonic,
				val: "SHUTDOWN_START",
			},
		},
	},
	{
		in: "%SYS-6-LOGGINGHOST_STARTSTOP: Logging to host 192.168.1.239 started - CLI initiated",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYS",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "6",
			},
			{
				typ: tokenVendorCiscoMnemonic,
				val: "LOGGINGHOST_STARTSTOP",
			},
		},
	},
	{
		in: "%SYS-21-LOGGINGHOST_STARTSTOP: Logging to host 192.168.1.239 started - CLI initiated",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYS",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "21",
			},
			{
				typ: tokenVendorCiscoMnemonic,
				val: "LOGGINGHOST_STARTSTOP",
			},
		},
	},
	{
		in: "%SYS-5-CONFIG_I: Configured from console by console",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYS",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "5",
			},
			{
				typ: tokenVendorCiscoMnemonic,
				val: "CONFIG_I",
			},
		},
	},
	{
		in: "%SYS-5: Configured from console by console",
		out: []token{
			{
				typ: tokenVendorCiscoFacility,
				val: "SYS",
			},
			{
				typ: tokenVendorCiscoSeverity,
				val: "5",
			},
		},
	},
}

// ciscoParseTestMessage defines unit tests for parsing a syslog messages and Cisco vendor structured data.
var ciscoParseTestMessages = [...]parseTest{
	{
		in:    "<0>1 2000-01-01T23:59:59.0001+08:00 sw1 - - - - %LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/0, changed state to down",
		valid: true,
		out: []SyslogRecord{
			{
				Version:   RFC5424,
				Facility:  0,
				Severity:  0,
				Timestamp: time.Time{},
				Host:      "sw1",
				Message:   "Line protocol on Interface GigabitEthernet0/0, changed state to down",
				Data: map[string]StructuredData{
					"vendor": {
						Id: "vendor",
						Params: []StructuredDataParam{
							{
								Key: "name",
								Val: "Cisco",
							},
							{
								Key: "facility",
								Val: "LINEPROTO",
							},
							{
								Key: "mnemonic",
								Val: "UPDOWN",
							},
						},
					},
				},
			},
		},
	},
	{
		in:    "<0> Apr  4 23:59:59 sw1 %LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/0, changed state to down",
		valid: true,
		out: []SyslogRecord{
			{
				Version:   RFC3164,
				Facility:  0,
				Severity:  0,
				Timestamp: time.Time{},
				Host:      "sw1",
				Message:   "Line protocol on Interface GigabitEthernet0/0, changed state to down",
				Data: map[string]StructuredData{
					"vendor": {
						Id: "vendor",
						Params: []StructuredDataParam{
							{
								Key: "name",
								Val: "Cisco",
							},
							{
								Key: "facility",
								Val: "LINEPROTO",
							},
							{
								Key: "mnemonic",
								Val: "UPDOWN",
							},
						},
					},
				},
			},
		},
	},
	{
		in:    "<0> Apr  4 23:59:59 sw1 %LINEPROTO-SUBFACILITY-5-UPDOWN: Line protocol on Interface GigabitEthernet0/0, changed state to down",
		valid: true,
		out: []SyslogRecord{
			{
				Version:   RFC3164,
				Facility:  0,
				Severity:  0,
				Timestamp: time.Time{},
				Host:      "sw1",
				Message:   "Line protocol on Interface GigabitEthernet0/0, changed state to down",
				Data: map[string]StructuredData{
					"vendor": {
						Id: "vendor",
						Params: []StructuredDataParam{
							{
								Key: "name",
								Val: "Cisco",
							},
							{
								Key: "facility",
								Val: "LINEPROTO",
							},
							{
								Key: "mnemonic",
								Val: "UPDOWN",
							},
						},
					},
				},
			},
		},
	},
}

// keyValueTestMessages defines unit tests for verifying syslog messages that've got key-value pairs
// stored in the Message part, defined as key=value or key:value.
var keyValueTestMessages = [...]parseTest{
	// Test 1. Expect no key-value pairs.
	{
		in:    "this message part has no key-value pairs",
		valid: true,
		out: []SyslogRecord{
			{
				Message: "this message part has no key-value pairs",
			},
		},
	},
	// Test 2. Verify parsing of key=value.
	{
		in:    "i've got some data for you: day=monday week=45 year=1970 and lastly, nudles=false",
		valid: true,
		out: []SyslogRecord{
			{
				Message: "i've got some data for you: day=monday week=45 year=1970 and lastly, nudles=false",
				Data: map[string]StructuredData{
					"key-value": {
						Id: "key-value",
						Params: []StructuredDataParam{
							{
								Key: "day",
								Val: "monday",
							},
							{
								Key: "week",
								Val: "45",
							},
							{
								Key: "year",
								Val: "1970",
							},
							{
								Key: "nudles",
								Val: "false",
							},
						},
					},
				},
			},
		},
	},
	// Test 3. Verify parsing of key:value.
	{
		in:    "i've got some data for you: day:monday week:45 year:1970 and lastly, nudles:false",
		valid: true,
		out: []SyslogRecord{
			{
				Message: "i've got some data for you: day:monday week:45 year:1970 and lastly, nudles:false",
				Data: map[string]StructuredData{
					"key-value": {
						Id: "key-value",
						Params: []StructuredDataParam{
							{
								Key: "day",
								Val: "monday",
							},
							{
								Key: "week",
								Val: "45",
							},
							{
								Key: "year",
								Val: "1970",
							},
							{
								Key: "nudles",
								Val: "false",
							},
						},
					},
				},
			},
		},
	},
	// Test 4. Verify parsing of key-value pairs with string literal enclosed keys and values.
	{
		in:    "i've got some data for you: \"day\":\"monday\" 'week':\"45\" 'year':'1970' and lastly, \"nudles\":'false'",
		valid: true,
		out: []SyslogRecord{
			{
				Message: "i've got some data for you: \"day\":\"monday\" 'week':\"45\" 'year':'1970' and lastly, \"nudles\":'false'",
				Data: map[string]StructuredData{
					"key-value": {
						Id: "key-value",
						Params: []StructuredDataParam{
							{
								Key: "day",
								Val: "monday",
							},
							{
								Key: "week",
								Val: "45",
							},
							{
								Key: "year",
								Val: "1970",
							},
							{
								Key: "nudles",
								Val: "false",
							},
						},
					},
				},
			},
		},
	},
	// Test 5. Verify parsing of key-value pairs with mixed string literal enclosed keys and values
	// using '_' and '-'.
	{
		in:    "i've got some data for you: \"day_of_week\"=monday week-in-calendar-year:\"45\" 'year'='1970' and lastly, \"nudle_type\":egg-nudles",
		valid: true,
		out: []SyslogRecord{
			{
				Message: "i've got some data for you: \"day_of_week\"=monday week-in-calendar-year:\"45\" 'year'='1970' and lastly, \"nudle_type\":egg-nudles",
				Data: map[string]StructuredData{
					"key-value": {
						Id: "key-value",
						Params: []StructuredDataParam{
							{
								Key: "day_of_week",
								Val: "monday",
							},
							{
								Key: "week-in-calendar-year",
								Val: "45",
							},
							{
								Key: "year",
								Val: "1970",
							},
							{
								Key: "nudle_type",
								Val: "egg-nudles",
							},
						},
					},
				},
			},
		},
	},
}

// cefTestMessages defines unit tests for verifying syslog messages that've got Common Event Format (CEF)
// messages stored in the Message part, defined by the 2010 ArcSight standard.
var cefTestMessages = [...]parseTest{
	{
		// Test 1: Only mandatory CEF prefix. No trailing key-value pairs.
		in:    "<0>1 2000-01-01T23:59:59.0001+08:00 sw1 - - - - CEF:0|cyber2000|switch9000|1|0x23abce|application crash|9",
		valid: true,
		out: []SyslogRecord{
			{
				Version: RFC5424,
				Host:    "sw1",
				Message: "CEF:0|cyber2000|switch9000|1|0x23abce|application crash|9",
				Data: map[string]StructuredData{
					"cef": {
						Id: "cef",
						Params: []StructuredDataParam{
							{
								Key: "cef-version",
								Val: "0",
							},
							{
								Key: "vendor",
								Val: "cyber2000",
							},
							{
								Key: "product",
								Val: "switch9000",
							},
							{
								Key: "version",
								Val: "1",
							},
							{
								Key: "signature",
								Val: "0x23abce",
							},
							{
								Key: "event name",
								Val: "application crash",
							},
							{
								Key: "severity",
								Val: "9",
							},
						},
					},
				},
			},
		},
	},
	{
		// Test 2: CEF with single trailing key-value pairs.
		in:    "<0>1 2000-01-01T23:59:59.0001+08:00 sw1 - - - - CEF:0|cyber2000|switch9000|1|0x23abce|application crash|9|key=value",
		valid: true,
		out: []SyslogRecord{
			{
				Version: RFC5424,
				Host:    "sw1",
				Message: "CEF:0|cyber2000|switch9000|1|0x23abce|application crash|9|key=value",
				Data: map[string]StructuredData{
					"cef": {
						Id: "cef",
						Params: []StructuredDataParam{
							{
								Key: "cef-version",
								Val: "0",
							},
							{
								Key: "vendor",
								Val: "cyber2000",
							},
							{
								Key: "product",
								Val: "switch9000",
							},
							{
								Key: "version",
								Val: "1",
							},
							{
								Key: "signature",
								Val: "0x23abce",
							},
							{
								Key: "event name",
								Val: "application crash",
							},
							{
								Key: "severity",
								Val: "9",
							},
							{
								Key: "key",
								Val: "value",
							},
						},
					},
				},
			},
		},
	},
	{
		// Test 3: CEF with multiple trailing key-value pairs.
		in:    "<0>1 2000-01-01T23:59:59.0001+08:00 sw1 - - - - CEF:0|cyber2000|switch9000|1|0x23abce|application crash|9|key=value|speed=100|altitude=9001",
		valid: true,
		out: []SyslogRecord{
			{
				Version: RFC5424,
				Host:    "sw1",
				Message: "CEF:0|cyber2000|switch9000|1|0x23abce|application crash|9|key=value|speed=100|altitude=9001",
				Data: map[string]StructuredData{
					"cef": {
						Id: "cef",
						Params: []StructuredDataParam{
							{
								Key: "cef-version",
								Val: "0",
							},
							{
								Key: "vendor",
								Val: "cyber2000",
							},
							{
								Key: "product",
								Val: "switch9000",
							},
							{
								Key: "version",
								Val: "1",
							},
							{
								Key: "signature",
								Val: "0x23abce",
							},
							{
								Key: "event name",
								Val: "application crash",
							},
							{
								Key: "severity",
								Val: "9",
							},
							{
								Key: "key",
								Val: "value",
							},
							{
								Key: "speed",
								Val: "100",
							},
							{
								Key: "altitude",
								Val: "9001",
							},
						},
					},
				},
			},
		},
	},
}

// ----------------------
// ----- Functions ------
// ----------------------

// ------------------
// ----- Tests ------
// ------------------

// ------------------------
// ----- Cisco tests ------
// ------------------------

// TestLexCisco verifies that the lexer correctly scans a valid Cisco syslog message part for Cisco facility, severity
// and mnemonic.
func TestLexCisco(t *testing.T) {
	for i1, e1 := range ciscoLexTestMessages {
		l := lexer{
			input:   e1.in,
			line:    1,
			linePos: 1,
			state:   lexVendorCiscoFacility,
		}
		for i2, e2 := range e1.out {
			tok := l.nextToken()
			if tok.typ != e2.typ {
				t.Errorf("test %d, token %d: expected token of type %s, got %s", i1+1, i2+1, e2.Type(), tok.Type())
			}
			if tok.val != e2.val {
				t.Errorf("test %d, token %d: expected token value %s, got %s", i1+1, i2+1, e2.Value(), tok.Value())
			}
		}
	}
}

// TestParseCisco fully verifies that the parser can parse a syslog message with Cisco vendor specific structured
// data elements.
func TestParseCisco(t *testing.T) {
	for i1, e1 := range ciscoParseTestMessages {
		slogs, err := Parse(e1.in)
		if err != nil && e1.valid {
			t.Errorf("test %d: expected valid parse, got error: %s", i1+1, err)
			continue
		}
		if len(slogs) != 1 {
			t.Errorf("test %d: expected valid parse, got no result", i1+1)
			continue
		}
		slog := slogs[0]
		DecodeCisco(&slog)
		out := e1.out[0]
		if slog.Version != out.Version {
			t.Errorf("test %d: expected version %d, got %d", i1+1, out.Version, slog.Version)
		}
		if slog.Facility != out.Facility {
			t.Errorf("test %d: expected facility %d, got %d", i1+1, out.Facility, slog.Facility)
		}
		if slog.Severity != out.Severity {
			t.Errorf("test %d: expected severity %d, got %d", i1+1, out.Severity, slog.Severity)
		}
		if slog.Host != out.Host {
			t.Errorf("test %d: expected host %q, got %q", i1+1, out.Host, slog.Host)
		}
		if slog.Message != out.Message {
			t.Errorf("test %d: expected message %q, got %q", i1+1, out.Message, slog.Message)
		}
		if len(slog.Data) != len(out.Data) {
			t.Errorf("test %d: expected %d structured data elements, got %d", i1+1, len(out.Data), len(slog.Data))
			continue
		}
		i2 := 0
		for _, e2 := range slog.Data {
			target, ok := e1.out[i2].Data[e2.Id]
			if !ok {
				t.Errorf("test %d, structured data element %d: did not get expected id %q",
					i1+1, i2+1, e2.Id)
				i2++
				continue
			}
			if len(target.Params) != len(e2.Params) {
				t.Errorf("test %d, structured data element %d: expected %d structured data parameters, got %d",
					i1+1, i2+1, len(e2.Params), len(target.Params))
				continue
			}
			for i3, e3 := range e2.Params {
				if target.Params[i3].Key != e3.Key {
					t.Errorf("test %d, structured data element %d, param %d: expected key %q, got %q",
						i1+1, i2+1, i3+1, e3.Key, target.Params[i3].Key)
				}
				if target.Params[i3].Val != e3.Val {
					t.Errorf("test %d, structured data element %d, param %d: expected value %q, got %q",
						i1+1, i2+1, i3+1, e3.Val, target.Params[i3].Val)
				}
			}
			i2++
		}
	}
}

// TestParseKeyValue verifies that parsing of key-value pairs within a syslog message, having either
// key=value or key:value.
func TestParseKeyValue(t *testing.T) {
	for i1, e1 := range keyValueTestMessages {
		sr := SyslogRecord{Message: e1.in}
		if err := ParseKeyValuePairs(&sr); err != nil {
			t.Errorf("test %d: expected valid key-value parse, got error: %s", i1+1, err)
			continue
		}
		if len(e1.out) != 1 && len(e1.out[0].Data) != len(sr.Data) {
			t.Errorf("test %d: expected single data element of key-value pairs, got %d", i1+1, len(sr.Data))
			continue
		}
		for k, v := range e1.out[0].Data {
			parsed, ok := sr.Data[k]
			if !ok {
				t.Errorf("test %d: expected data ID %s, got none", i1+1, k)
				continue
			}
			if len(v.Params) != len(parsed.Params) {
				t.Errorf("test %d: expected %d key-value pairs, got %d", i1+1, len(v.Params), len(parsed.Params))
				continue
			}
			for i3, e3 := range v.Params {
				if e3.Key != parsed.Params[i3].Key {
					t.Errorf("test %d, pair %d: expected key '%s', got '%s'", i1+1, i3+1, e3.Key, parsed.Params[i3].Key)
					continue
				}
				if e3.Val != parsed.Params[i3].Val {
					t.Errorf("test %d, pair %d: expected value '%s', got '%s'", i1+1, i3+1, e3.Val, parsed.Params[i3].Val)
					continue
				}
			}
		}
	}
}

// TestParseCEF verifies that parsing of Common Event Format (CEF) within a syslog message, per the 2010 ArcSight
// CEF standard.
func TestParseCEF(t *testing.T) {
	for i1, e1 := range cefTestMessages {
		slogs, err := Parse(e1.in)
		if err != nil && e1.valid {
			t.Errorf("test %d: expected valid parse, got error: %s", i1+1, err)
			continue
		}
		if len(slogs) != 1 {
			t.Errorf("test %d: expected valid parse, got no result", i1+1)
			continue
		}
		sr := slogs[0]
		if err := ParseCEF(&sr); err != nil {
			t.Errorf("test %d: expected valid CEF parse, got error: %s", i1+1, err)
			continue
		}
		if len(e1.out) != 1 && len(e1.out[0].Data) != len(sr.Data) {
			t.Errorf("test %d: expected single data element of CEF key-value pairs, got %d", i1+1, len(sr.Data))
			continue
		}
		for k, v := range e1.out[0].Data {
			parsed, ok := sr.Data[k]
			if !ok {
				t.Errorf("test %d: expected data ID %s, got none", i1+1, k)
				continue
			}
			if len(v.Params) != len(parsed.Params) {
				t.Errorf("test %d: expected %d key-value pairs, got %d", i1+1, len(v.Params), len(parsed.Params))
				continue
			}
			for i3, e3 := range v.Params {
				if e3.Key != parsed.Params[i3].Key {
					t.Errorf("test %d, pair %d: expected key '%s', got '%s'", i1+1, i3+1, e3.Key, parsed.Params[i3].Key)
					continue
				}
				if e3.Val != parsed.Params[i3].Val {
					t.Errorf("test %d, pair %d: expected value '%s', got '%s'", i1+1, i3+1, e3.Val, parsed.Params[i3].Val)
					continue
				}
			}
		}
	}
}
