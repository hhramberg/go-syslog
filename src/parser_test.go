// parser_test.go provides unit tests for verifying that the syslog parser correctly takes a token stream and
// constructs SyslogRecords from either RFC5424 or RFC3164 source message.
//
// In addition to unit tests the parser_test.go file provides benchmarks for measuring the performance of the
// syslog parsing implementation.

package src

import (
	"strconv"
	"strings"
	"testing"
	"time"
)

// ----------------------------
// ----- Type definitions -----
// ----------------------------

// parseTest wraps the input data stream, a valid tag and the expected output syslog entry.
type parseTest struct {
	in    string         // The input string to parse.
	valid bool           // Set to true if this test unit is expected to be parsed correctly, else false.
	out   []SyslogRecord // The expected syslog message after parsing.
}

// parseTestTimestamp defines a test for testing timestamp parsing.
type parseTestTimestamp struct {
	in  string
	out time.Time
}

// ---------------------
// ----- Constants -----
// ---------------------

// --------------------
// ----- Globals ------
// --------------------

// parseTests5424 holds test data for verifying valid RFC5424 parse operation.
var parseTests5424 = [...]parseTest{
	{
		in:    "<34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - 'su root' failed for lonvick on /dev/pts/8",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 4,
			Severity: 2,
			Host:     "mymachine.example.com",
			Message:  "'su root' failed for lonvick on /dev/pts/8",
			Data:     nil,
		}},
	},
	{
		in:    "<165>1 2003-08-24T05:14:15.000003-07:00 192.0.2.1 myproc 8710 - - %% It's time to make the do-nuts.",
		valid: true,
		out: []SyslogRecord{
			{
				Version:  RFC5424,
				Facility: 20,
				Severity: 5,
				Host:     "192.0.2.1",
				Message:  "%% It's time to make the do-nuts.",
				Data:     nil,
			},
		},
	},
	{
		in:    "<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut=\"3\" eventSource=\"Application\" eventID=\"1011\"] BOMAn application event log entry...",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 20,
			Severity: 5,
			Host:     "mymachine.example.com",
			Message:  "BOMAn application event log entry...",
			AppName:  "evntslog",
			Data: map[string]StructuredData{
				"exampleSDID@32473": {
					Id: "exampleSDID@32473",
					Params: []StructuredDataParam{
						{
							Key: "iut",
							Val: "3",
						},
						{
							Key: "eventSource",
							Val: "Application",
						},
						{
							Key: "eventID",
							Val: "1011",
						},
					},
				},
			},
		}},
	},
	{
		in:    "<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut=\"3\" eventSource=\"Application\" eventID=\"1011\"][examplePriority@32473 class=\"high\"]",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 20,
			Severity: 5,
			Host:     "mymachine.example.com",
			Message:  "",
			Data: map[string]StructuredData{
				"exampleSDID@32473": {
					Id: "exampleSDID@32473",
					Params: []StructuredDataParam{
						{
							Key: "iut",
							Val: "3",
						},
						{
							Key: "eventSource",
							Val: "Application",
						},
						{
							Key: "eventID",
							Val: "1011",
						},
					},
				},
				"examplePriority@32473": {
					Id: "examplePriority@32473",
					Params: []StructuredDataParam{
						{
							Key: "class",
							Val: "high",
						},
					},
				},
			},
		}},
	},
	{
		// Maximum length of all fields. No structured data, no message.
		in: "<191>1 2003-08-24T05:14:15.000003-07:00 " +
			// Hostname of length 255 characters.
			"12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
			"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
			"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com " +
			// Application name of length 48 characters.
			"abcdefghijklmnopqrstuvwxyz1234567890applications " +
			// Process id of length 128 characters.
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_" +
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ_12345678901234567890 " +
			// Message id of length 32 characters.
			"MESSAGEID123456789_SECRETMESSAGE " +
			// Structured data.
			"- " +
			// Message.
			"-",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 23,
			Severity: 7,
			Host: "12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
				"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
				"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com",
			Message: "",
			Data:    map[string]StructuredData{},
		}},
	},
	{
		// Maximum length of all fields. Structured data ID differ in the last letter.
		in: "<191>1 2003-08-24T05:14:15.000003-07:00 " +
			// Hostname of length 255 characters.
			"12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
			"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
			"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com " +
			// Application name of length 48 characters.
			"abcdefghijklmnopqrstuvwxyz1234567890applications " +
			// Process id of length 128 characters.
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_" +
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ_12345678901234567890 " +
			// Message id of length 32 characters.
			"MESSAGEID123456789_SECRETMESSAGE " +
			// Structured data.
			"[examplesd-id@thatis32characters! " +
			"example-param-name-thats-sd-name=\"short\" " +
			"example-param-name-thats-sd-name=\"short param value\" " +
			"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
			"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
			"longer than the previous longester parameter values, really looooooong\" " +
			"] " +
			"[examplesd-id@thatis32characterz! " +
			"example-param-name-thats-sd-name=\"short\" " +
			"example-param-name-thats-sd-name=\"short param value\" " +
			"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
			"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
			"longer than the previous longester parameter values, really looooooong\" " +
			"] " +
			"[examplesd-id@thatis32characterc! " +
			"example-param-name-thats-sd-name=\"short\" " +
			"example-param-name-thats-sd-name=\"short param value\" " +
			"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
			"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
			"longer than the previous longester parameter values, really looooooong\" " +
			"] " +
			"[examplesd-id@thatis32characterv! " +
			"example-param-name-thats-sd-name=\"short\" " +
			"example-param-name-thats-sd-name=\"short param value\" " +
			"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
			"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
			"longer than the previous longester parameter values, really looooooong\" " +
			"] " +
			"[examplesd-id@thatis32characterb! " +
			"example-param-name-thats-sd-name=\"short\" " +
			"example-param-name-thats-sd-name=\"short param value\" " +
			"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
			"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
			"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
			"longer than the previous longester parameter values, really looooooong\" " +
			"] " +
			// Message.
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
			"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
			"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
			"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
			"deserunt mollit anim id est laborum.",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 23,
			Severity: 7,
			Host: "12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
				"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
				"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com",
			Message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " +
				"labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
				"nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit " +
				"esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt " +
				"in culpa qui officia deserunt mollit anim id est laborum.",
			Data: map[string]StructuredData{
				"examplesd-id@thatis32characters!": {
					Id: "examplesd-id@thatis32characters!",
					Params: []StructuredDataParam{
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short param value",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "longer parameter value than b4",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer even, basically",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer than the previous parameter values.",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "loooooongestest very long super not short parameter value, muuuuuch longer than " +
								"the previous longester parameter values, really looooooong",
						},
					},
				},
				"examplesd-id@thatis32characterz!": {
					Id: "examplesd-id@thatis32characters!",
					Params: []StructuredDataParam{
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short param value",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "longer parameter value than b4",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer even, basically",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer than the previous parameter values.",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "loooooongestest very long super not short parameter value, muuuuuch longer than " +
								"the previous longester parameter values, really looooooong",
						},
					},
				},
				"examplesd-id@thatis32characterc!": {
					Id: "examplesd-id@thatis32characters!",
					Params: []StructuredDataParam{
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short param value",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "longer parameter value than b4",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer even, basically",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer than the previous parameter values.",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "loooooongestest very long super not short parameter value, muuuuuch longer than " +
								"the previous longester parameter values, really looooooong",
						},
					},
				},
				"examplesd-id@thatis32characterv!": {
					Id: "examplesd-id@thatis32characters!",
					Params: []StructuredDataParam{
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short param value",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "longer parameter value than b4",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer even, basically",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer than the previous parameter values.",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "loooooongestest very long super not short parameter value, muuuuuch longer than " +
								"the previous longester parameter values, really looooooong",
						},
					},
				},
				"examplesd-id@thatis32characterb!": {
					Id: "examplesd-id@thatis32characters!",
					Params: []StructuredDataParam{
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "short param value",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "longer parameter value than b4",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer even, basically",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "this parameter value is longer than the previous parameter values.",
						},
						{
							Key: "example-param-name-thats-sd-name",
							Val: "loooooongestest very long super not short parameter value, muuuuuch longer than " +
								"the previous longester parameter values, really looooooong",
						},
					},
				},
			},
		},
		}},
	{
		in:    "<0>1 - - - - - -",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 0,
			Severity: 0,
			Host:     "",
			Message:  "",
			Data:     nil,
		}},
	},
	{
		in:    "<0>1 2003-08-24T05:14:15Z - - - - -",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 0,
			Severity: 0,
			Host:     "",
			Message:  "",
			Data:     nil,
		}},
	},
	{
		in:    "<0>1 - - - - - [exampleSDID@32473 iut=\"3\" eventSource=\"Application\" eventID=\"1011\"][examplePriority@32473 class=\"high\"] -",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 0,
			Severity: 0,
			Host:     "",
			Message:  "",
			Data: map[string]StructuredData{
				"exampleSDID@32473": {
					Id: "exampleSDID@32473",
					Params: []StructuredDataParam{
						{
							Key: "iut",
							Val: "3",
						},
						{
							Key: "eventSource",
							Val: "Application",
						},
						{
							Key: "eventID",
							Val: "1011",
						},
					},
				},
				"examplePriority@32473": {
					Id: "examplePriority@32473",
					Params: []StructuredDataParam{
						{
							Key: "class",
							Val: "high",
						},
					},
				},
			},
		}},
	},
	{
		in: "<190>1 2003-08-24T05:14:15.000003-07:00 san01-cluster.field-cluster.corporate-san.com squalidMessages " +
			"processID1234 0xab726ad723da5f4e - this sample message is very long ... unnecessarily long actually. " +
			"PLEASE STOP NOW. No, I won't, because this message has to be long",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC5424,
			Facility: 23,
			Severity: 6,
			Host:     "san01-cluster.field-cluster.corporate-san.com",
			Message: "this sample message is very long ... unnecessarily long actually. " +
				"PLEASE STOP NOW. No, I won't, because this message has to be long",
			Data: nil,
		}},
	},
	{
		in: "107 <34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - 'su root' failed for lonvick " +
			"on /dev/pts/8" +
			"107 <34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - 'su root' failed for lonvick " +
			"on /dev/pts/8",
		valid: true,
		out: []SyslogRecord{
			{
				Version:  RFC5424,
				Facility: 4,
				Severity: 2,
				Host:     "mymachine.example.com",
				Message:  "'su root' failed for lonvick on /dev/pts/8",
				Data:     nil,
			},
			{
				Version:  RFC5424,
				Facility: 4,
				Severity: 2,
				Host:     "mymachine.example.com",
				Message:  "'su root' failed for lonvick on /dev/pts/8",
				Data:     nil,
			}},
	},
	{
		in: "107 <34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - 'su root' failed for lonvick " +
			"on /dev/pts/8" +
			"99 <165>1 2003-08-24T05:14:15.000003-07:00 192.0.2.1 myproc 8710 - - %% It's time to make the do-nuts." +
			"35 <0>1 2003-08-24T05:14:15Z - - - - -",
		valid: true,
		out: []SyslogRecord{
			{
				Version:  RFC5424,
				Facility: 4,
				Severity: 2,
				Host:     "mymachine.example.com",
				Message:  "'su root' failed for lonvick on /dev/pts/8",
				Data:     nil,
			},
			{
				Version:  RFC5424,
				Facility: 20,
				Severity: 5,
				Host:     "192.0.2.1",
				Message:  "%% It's time to make the do-nuts.",
				Data:     nil,
			}, {
				Version:  RFC5424,
				Facility: 0,
				Severity: 0,
				Host:     "",
				Message:  "",
				Data:     nil,
			},
		},
	},
}

// parseTests3164 holds test data for verifying valid RFC3164 parse operation.
var parseTests3164 = [...]parseTest{
	{
		// Test 1.
		in:    "<34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 4,
			Severity: 2,
			Timestamp: time.Date(
				0,
				10,
				11,
				22,
				14,
				15,
				0,
				time.UTC,
			),
			Host:    "mymachine",
			AppName: "su",
			Message: "'su root' failed for lonvick on /dev/pts/8",
		}},
	},
	{
		// Test 2.
		in:    "Use the BFG!",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 1,
			Severity: 5,
			Host:     "Use",
			Message:  "the BFG!",
		}},
	},
	{
		// Test 3.
		in:    "<13>Feb  5 17:32:18 10.0.0.99 Use the BFG!",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 1,
			Severity: 5,
			Timestamp: time.Date(
				0,
				2,
				5,
				17,
				32,
				18,
				0,
				time.UTC,
			),
			Host:    "10.0.0.99",
			Message: "Use the BFG!",
		}},
	},
	{
		// Test 4.
		in:    "<165>Aug 24 05:34:00 CST 1987 mymachine myproc[10]: %% It's time to make the do-nuts.  %%  Ingredients: Mix=OK, Jelly=OK # Devices: Mixer=OK, Jelly_Injector=OK, Frier=OK # Transport: Conveyer1=OK, Conveyer2=OK # %%",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 20,
			Severity: 5,
			Timestamp: time.Date(
				0,
				8,
				24,
				05,
				34,
				0,
				0,
				time.UTC,
			),
			Host:    "CST",
			Message: "1987 mymachine myproc[10]: %% It's time to make the do-nuts.  %%  Ingredients: Mix=OK, Jelly=OK # Devices: Mixer=OK, Jelly_Injector=OK, Frier=OK # Transport: Conveyer1=OK, Conveyer2=OK # %%",
		}},
	},
	{
		// Test 5.
		in:    "<0>1990 Oct 22 10:52:01 TZ-6 scapegoat.dmz.example.org 10.1.2.3 sched[0]: That's All Folks!",
		valid: false,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 0,
			Severity: 0,
			Host:     "1990",
			Message:  "Oct 22 10:52:01 TZ-6 scapegoat.dmz.example.org 10.1.2.3 sched[0]: That's All Folks!",
		}},
	},
	{
		// Test 6.
		in:    "<165>Oct 11 12:34:56 db02-datastore hard disks are nearly full, usage is 90%",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 20,
			Severity: 5,
			Timestamp: time.Date(
				0,
				10,
				11,
				12,
				34,
				56,
				0,
				time.UTC,
			),
			Host:    "db02-datastore",
			Message: "hard disks are nearly full, usage is 90%",
		}},
	},
	{
		// Test 7.
		in:    "Oct 11 12:34:56",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 1,
			Severity: 5,
			Timestamp: time.Date(
				0,
				10,
				11,
				12,
				34,
				56,
				0,
				time.UTC,
			),
		}},
	},
	{
		// Test 8.
		in:    "Oct  1 12:34:56",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 1,
			Severity: 5,
			Timestamp: time.Date(
				0,
				10,
				1,
				12,
				34,
				56,
				0,
				time.UTC,
			),
		}},
	},
	{
		// Test 9.
		in:    "host",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 1,
			Severity: 5,
			Host:     "host",
		}},
	},
	{
		// Test 10.
		in: "<191>Oct 11 22:14:15 " +
			// Host.
			"a_hostname_of_63_characters_that_complies_with_the_rfc1034_std_ " +
			// Message.
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
			"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
			"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
			"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
			"deserunt mollit anim id est laborum. " +
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
			"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
			"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
			"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
			"deserunt mollit anim id est laborum.",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 23,
			Severity: 7,
			Timestamp: time.Date(
				0,
				10,
				11,
				22,
				14,
				15,
				0,
				time.UTC,
			),
			Host: "a_hostname_of_63_characters_that_complies_with_the_rfc1034_std_",
			Message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
				"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
				"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
				"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
				"deserunt mollit anim id est laborum. " +
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
				"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
				"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
				"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
				"deserunt mollit anim id est laborum.",
			Data: nil,
		}},
	},
	{
		// Test 11.
		in: "76 <34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8" +
			"76 <34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8",
		valid: true,
		out: []SyslogRecord{
			{
				Version:  RFC3164,
				Facility: 4,
				Severity: 2,
				Timestamp: time.Date(
					0,
					10,
					11,
					22,
					14,
					15,
					0,
					time.UTC,
				),
				Host:    "mymachine",
				AppName: "su",
				Message: "'su root' failed for lonvick on /dev/pts/8",
			},
			{
				Version:  RFC3164,
				Facility: 4,
				Severity: 2,
				Timestamp: time.Date(
					0,
					10,
					11,
					22,
					14,
					15,
					0,
					time.UTC,
				),
				Host:    "mymachine",
				AppName: "su",
				Message: "'su root' failed for lonvick on /dev/pts/8",
			},
		},
	},
	{
		// Test 12.
		in: "76 <34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8" +
			"42 <13>Feb  5 17:32:18 10.0.0.99 Use the BFG!" +
			"15 Oct  1 12:34:56",
		valid: true,
		out: []SyslogRecord{
			{
				Version:  RFC3164,
				Facility: 4,
				Severity: 2,
				Timestamp: time.Date(
					0,
					10,
					11,
					22,
					14,
					15,
					0,
					time.UTC,
				),
				Host:    "mymachine",
				AppName: "su",
				Message: "'su root' failed for lonvick on /dev/pts/8",
			},
			{
				Version:  RFC3164,
				Facility: 1,
				Severity: 5,
				Timestamp: time.Date(
					0,
					2,
					5,
					17,
					32,
					18,
					0,
					time.UTC,
				),
				Host:    "10.0.0.99",
				Message: "Use the BFG!",
			},
			{
				Version:  RFC3164,
				Facility: 1,
				Severity: 5,
				Timestamp: time.Date(
					0,
					10,
					1,
					12,
					34,
					56,
					0,
					time.UTC,
				),
			},
		},
	},
	{
		// Test 13. Test minimal RFC3164 compatible message with PRI part only.
		in:    "<0>",
		valid: true,
		out: []SyslogRecord{{
			Version: RFC3164,
		}},
	},
	{
		// Test 14. Test minimal RFC3164 compatible message with PRI part only.
		in:    "<191>",
		valid: true,
		out: []SyslogRecord{{
			Version:  RFC3164,
			Facility: 23,
			Severity: 7,
		}},
	},
}

// parseTestsTimestamps5424 holds timestamp data for test data. This must be parsed at run-time because
// Go can't parse time at compile time.
var parseTestsTimestamps5424 = [...][]string{
	{"2003-10-11T22:14:15.003Z"},
	{"2003-08-24T05:14:15.000003-07:00"},
	{"2003-10-11T22:14:15.003Z"},
	{"2003-10-11T22:14:15.003Z"},
	{"2003-08-24T05:14:15.000003-07:00"},
	{"2003-08-24T05:14:15.000003-07:00"},
	{""},
	{"2003-08-24T05:14:15Z"},
	{""},
	{"2003-08-24T05:14:15.000003-07:00"},
	{"2003-10-11T22:14:15.003Z", "2003-10-11T22:14:15.003Z"},
	{"2003-10-11T22:14:15.003Z", "2003-08-24T05:14:15.000003-07:00", "2003-08-24T05:14:15Z"},
}

// parseTestsTimestamps3164 holds timestamp data for test data. This must be parsed at run-time because
// Go can't parse time at compile time.
var parseTestsTimestamps3164 = [...][]string{
	{"Oct 11 22:14:15"},
	{""},
	{"Feb  5 17:32:18"},
	{"Aug 24 05:34:00"},
	{""},
	{"Oct 11 12:34:56"},
	{"Oct 11 12:34:56"},
	{"Oct  1 12:34:56"},
	{""},
	{"Oct 11 22:14:15"},
	{"Oct 11 22:14:15", "Oct 11 22:14:15"},
	{"Oct 11 22:14:15", "Feb  5 17:32:18", "Oct  1 12:34:56"},
}

// parseTestsRFC4646 defines tests for verifying the correct parsing
// of RFC5424 section 7 IANA lang tags, defined in RFC4646.
var parseTestsRFC4646 = [...]struct {
	in    string
	valid bool
}{
	{
		// Test 1.
		in:    "",
		valid: false,
	},
	{
		// Test 2.
		in:    "a",
		valid: false,
	},
	{
		// Test 3.
		in:    " ",
		valid: false,
	},
	{
		// Test 4.
		in:    "-",
		valid: false,
	},
	{
		// Test 5.
		in:    "_",
		valid: false,
	},
	{
		// Test 6. Only langtag -> language.
		in:    "en",
		valid: true,
	},
	{
		// Test 7. Only language + extlang.
		in:    "en-fut-ure-use",
		valid: true,
	},
	{
		// Test 8. language + script (Zyyy means "uncetermined script").
		in:    "en-Zyyy",
		valid: true,
	},
	{
		// Test 9. language + region.
		in:    "en-UK",
		valid: true,
	},
	{
		// Test 10. language + region (digit form).
		in:    "en-826",
		valid: true,
	},
	{
		// Test 11. language + variant 1.
		in:    "en-varia",
		valid: true,
	},
	{
		// Test 12. language + variant 2.
		in:    "en-variants",
		valid: true,
	},
	{
		// Test 13. language + variant 3.
		in:    "en-4len",
		valid: true,
	},
	{
		// Test 14. language + variant 4.
		in:    "en-8lengths",
		valid: true,
	},
	{
		// Test 15. language + single extension.
		in:    "en-a-an",
		valid: true,
	},
	{
		// Test 16. language + multiple extensions.
		in:    "en-a-many-exten-sions",
		valid: true,
	},
	{
		// Test 17.
		in:    "fr-Latn-CA",
		valid: true,
	},
	{
		// Test 18.
		in:    "de-CH-1901",
		valid: true,
	},
	{
		// Test 19.
		in:    "g-grand-fathered",
		valid: true,
	},
	{
		// Test 20.
		in:    "en-US-x-private-use",
		valid: true,
	},
	{
		// Test 21.
		in:    "x-private-use",
		valid: true,
	},
}

// parseTests3164timestamps defines timestamp parsing tests that verifies that the RFC3164 timestamp
// parser succesfully complies with the Go time.Parse() function.
var parseTests3164timestamps = [...]parseTestTimestamp{
	{
		// Test 1.
		in:  "Jan  1 00:00:00",
		out: time.Date(0, 1, 1, 0, 0, 0, 0, time.UTC),
	},
	{
		// Test 2.
		in:  "Jan 10 01:00:00",
		out: time.Date(0, 1, 10, 1, 0, 0, 0, time.UTC),
	},
	{
		// Test 3.
		in:  "Jan 31 23:59:59",
		out: time.Date(0, 1, 31, 23, 59, 59, 0, time.UTC),
	},
	{
		// Test 4.
		in:  "Jan 31 23:59:59.123",
		out: time.Date(0, 1, 31, 23, 59, 59, 123, time.UTC),
	},
	{
		// Test 5.
		in:  "Jan 31 23:59:59.123 2001",
		out: time.Date(2001, 1, 31, 23, 59, 59, 123, time.UTC),
	},
	{
		// Test 6.
		in:  "Jan 31 23:59:59.123 2001 UTC",
		out: time.Date(2001, 1, 31, 23, 59, 59, 123, time.UTC),
	},
}

// ------------------
// ----- Tests ------
// ------------------

// TestParse5424 validates that the Parse function correctly parses a single RFC5424 message data stream.
func TestParse5424(t *testing.T) {
	// Set timestamps of output.
	for i1, e1 := range parseTestsTimestamps5424 {
		if len(e1) != len(parseTests5424[i1].out) {
			t.Fatalf("RFC5424 test entry %d: expected %d timestamps, got %d",
				i1+1, len(parseTests5424[i1].out), len(e1))
		}
		for i2, e2 := range e1 {
			if ts, err := time.Parse(time.RFC3339Nano, e2); err != nil && i1 != 6 && i1 != 8 {
				t.Fatalf("could not parse %q as a valid RFC3339 timestamp", e2)
			} else {
				parseTests5424[i1].out[i2].Timestamp = ts
			}
		}
	}

	// Run tests.
	for i1, e1 := range parseTests5424 {
		if slog, err := Parse(e1.in); err != nil && e1.valid {
			t.Errorf("RFC5424 test entry %d: expected valid parse, got error: %s", i1+1, err)
		} else {
			if len(slog) != len(e1.out) {
				t.Errorf("RFC5424 test entry %d: expected %d syslog entry, got %d", i1+1, len(e1.out), len(slog))
				continue
			}
			for i2, e2 := range slog {
				if e2.Version != e1.out[i2].Version {
					t.Errorf("RFC5424 test entry %d: expected version %d, got %d", i1+1, e1.out[i2].Version, e2.Version)
				}
				if e2.Facility != e1.out[i2].Facility {
					t.Errorf("RFC5424 test entry %d: expected facility %d, got %d", i1+1, e1.out[i2].Facility, e2.Facility)
				}
				if e2.Severity != e1.out[i2].Severity {
					t.Errorf("RFC5424 test entry %d: expected severity %d, got %d", i1+1, e1.out[i2].Severity, e2.Severity)
				}
				if !e2.Timestamp.Equal(e1.out[i2].Timestamp) && i1 != 6 && i1 != 8 {
					t.Errorf("RFC5424 test entry %d: expected timestamp %s, got %s",
						i1+1, e1.out[i2].Timestamp.String(), e2.Timestamp.String())
				}
				if e2.Host != e1.out[i2].Host {
					t.Errorf("RFC5424 test entry %d: expected host %q, got %q", i1+1, e1.out[i2].Host, e2.Host)
				}
				if e2.Message != e1.out[i2].Message {
					t.Errorf("RFC5424 test entry %d: expected message %q, got %q", i1+1, e1.out[i2].Message, e2.Message)
				}
				if len(e2.Data) == len(e1.out[i2].Data) {
					i3 := 0
					for _, e3 := range e1.out[i2].Data {
						target, ok := e2.Data[e3.Id]
						if !ok {
							t.Errorf(
								"RFC5424 test entry %d, sd element %d: strucutred data element Id %q not found",
								i1+1, i3+1, e3.Id)
							i3++
							continue
						}
						if len(e3.Params) == len(target.Params) {
							for i4, e4 := range e3.Params {
								if e4.Key != target.Params[i4].Key {
									t.Errorf("RFC5424 test entry %d, sd element %d, param %d: expected Key %q, got %q",
										i1+1, i3+1, i4+1, e4.Key, target.Params[i4].Key)
								}
								if e4.Val != target.Params[i4].Val {
									t.Errorf("RFC5424 test entry %d, sd element %d, param %d: expected value %q, got %q",
										i1+1, i3+1, i4+1, e4.Val, target.Params[i4].Val)
								}
							}
						} else {
							t.Errorf("RFC5424 test entry %d, sd element %d: expected structured data of length %d, got %d",
								i1+1, i3+1, len(e3.Params), len(target.Params))
						}
						i3++
					}
				} else {
					t.Errorf("RFC5424 test entry %d: expected structured data of length %d, got %d",
						i1+1, len(e1.out[i2].Data), len(e2.Data))
				}
			}
		}
	}
}

// TestParse3164 validates that the Parse function correctly parses a single RFC3164 message data stream.
func TestParse3164(t *testing.T) {
	// Set timestamps of output.
	// for i1, e1 := range parseTestsTimestamps3164 {
	// 	if len(e1) != len(parseTests3164[i1].out) {
	// 		t.Fatalf("RFC3164 test entry %d: expected %d timestamps, got %d",
	// 			i1+1, len(parseTests3164[i1].out), len(e1))
	// 	}
	// 	for i2, e2 := range e1 {
	// 		if ts, err := time.Parse(time.Stamp, e2); err != nil && i1 != 1 && i1 != 4 && i1 != 8 {
	// 			t.Fatalf("could not parse %q as a valid RFC3164 timestamp", e1)
	// 		} else if i1 != 1 && i1 != 4 {
	// 			ts = ts.AddDate(time.Now().Year(), 0, 0)
	// 			parseTests3164[i1].out[i2].Timestamp = ts
	// 		}
	// 	}
	// }

	// Run tests.
	for i1, e1 := range parseTests3164 {
		if slog, err := Parse(e1.in); err != nil && e1.valid {
			t.Errorf("RFC3164 test entry %d: expected valid parse, got error: %s", i1+1, err)
		} else {
			if len(slog) != len(e1.out) {
				t.Errorf("RFC3164 test entry %d: expected %d syslog entry, got %d", i1+1, len(e1.out), len(slog))
				continue
			}
			for i2, e2 := range slog {
				if e2.Version != e1.out[i2].Version {
					t.Errorf("RFC3164 test entry %d: expected version %d, got %d", i1+1, e1.out[i2].Version, e2.Version)
				}
				if e2.Facility != e1.out[i2].Facility {
					t.Errorf("RFC3164 test entry %d: expected facility %d, got %d", i1+1, e1.out[i2].Facility, e2.Facility)
				}
				if e2.Severity != e1.out[i2].Severity {
					t.Errorf("RFC3164 test entry %d: expected severity %d, got %d", i1+1, e1.out[i2].Severity, e2.Severity)
				}
				if !e2.Timestamp.Equal(e1.out[i2].Timestamp) && i1 != 6 && i1 != 8 {
					t.Errorf("RFC3164 test entry %d: expected timestamp %s, got %s",
						i1+1, e1.out[i2].Timestamp.String(), e2.Timestamp.String())
				}
				if e2.Host != e1.out[i2].Host {
					t.Errorf("RFC3164 test entry %d: expected host %q, got %q", i1+1, e1.out[i2].Host, e2.Host)
				}
				if e2.Message != e1.out[i2].Message {
					t.Errorf("RFC3164 test entry %d: expected message %q, got %q", i1+1, e1.out[i2].Message, e2.Message)
				}
				if len(e2.Data) == len(e1.out[i2].Data) {
					i3 := 0
					for _, e3 := range e1.out[i2].Data {
						target, ok := e2.Data[e3.Id]
						if !ok {
							t.Errorf(
								"RFC5424 test entry %d, sd element %d: strucutred data element Id %q not found",
								i1+1, i3+1, e3.Id)
							i3++
							continue
						}
						if len(e3.Params) == len(target.Params) {
							for i4, e4 := range e3.Params {
								if e4.Key != target.Params[i4].Key {
									t.Errorf("RFC3164 test entry %d, sd element %d, param %d: expected Key %q, got %q",
										i1+1, i3+1, i4+1, e4.Key, target.Params[i4].Key)
								}
								if e4.Val != target.Params[i4].Val {
									t.Errorf("RFC3164 test entry %d, sd element %d, param %d: expected value %q, got %q",
										i1+1, i3+1, i4+1, e4.Val, target.Params[i4].Val)
								}
							}
						} else {
							t.Errorf("RFC3164 test entry %d, sd element %d: expected structured data of length %d, got %d",
								i1+1, i3+1, len(e3.Params), len(target.Params))
						}
						i3++
					}
				} else {
					t.Errorf("RFC3164 test entry %d: expected structured data of length %d, got %d",
						i1+1, len(e1.out[i2].Data), len(e2.Data))
				}
			}
		}
	}
}

// TestParse4646 validates that the VerifyIANA function correctly verifies RFC4646 language tags.
func TestParse4646(t *testing.T) {
	for i1, e1 := range parseTestsRFC4646 {
		l := lexer{
			input: e1.in,
			line:  1,
			end:   len(e1.in),
			state: lexLanguage,
		}
		// call parseLanguage, the RFC4646 component of the VerifyIANA function.
		if err := parseLanguage(&l); err != nil && e1.valid {
			t.Errorf("test %d: %s", i1+1, err)
		}
	}
}

// ------------------------------
// ----- RFC5424 benchmarks -----
// ------------------------------

// Benchmark5424Minimal benchmarks parsing with the following message properties.
//
// Minimum PRIVAL length.
// No timestamp.
// No Hostname.
// No AppName.
// No ProcessID.
// No MessageID.
// No structured data.
// No message.
func Benchmark5424Minimal(b *testing.B) {
	msg := "<0>1 - - - - - -"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark5424MinTimestamp benchmarks parsing with the following message properties.
//
// Minimum PRIVAL length.
// Minimum timestamp length.
// No Hostname.
// No AppName.
// No ProcessID.
// No MessageID.
// No structured data.
// No message.
func Benchmark5424MinTimestamp(b *testing.B) {
	msg := "<0>1 2003-08-24T05:14:15Z - - - - -"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark5424StructuredData benchmarks parsing with the following message properties.
//
// Minimum PRIVAL length.
// No timestamp.
// No Hostname.
// No AppName.
// No ProcessID.
// No MessageID.
// Two structured elements with 4 and 5 parameters.
// No message.
func Benchmark5424StructuredData(b *testing.B) {
	msg := "<0>1 - - - - - [exampleid1 \"a\"=\"abba\" \"class\"=\"massive-hyper-collider\" " +
		"\"location\"=\"Dr. Eggman's secret laboratory\" \"model\"=\"653.ab\"][exampleid2 \"b\"=\"baab\" " +
		"\"cluster\"=\"phoenix2\" \"driver\"=\"420.ax\" \"rack\"=\"32\" \"shelf\"=\"11\"] -"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark5424MassiveDataNoStructured benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum timestamp length.
// Hostname length 45.
// AppName length 15.
// ProcessID length 13.
// MessageID length 18.
// No structured data.
// Message length 131.
func Benchmark5424MassiveDataNoStructured(b *testing.B) {
	msg := "<190>1 2003-08-24T05:14:15.000003-07:00 san01-cluster.field-cluster.corporate-san.com squalidMessages " +
		"processID1234 0xab726ad723da5f4e - this sample message is very long ... unnecessarily long actually. " +
		"PLEASE STOP NOW. No, I won't, because this message has to be long"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark5424MassiveDataNoStructured benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum timestamp length.
// Hostname length 45.
// AppName length 15.
// ProcessID length 13.
// MessageID length 18.
// Two structured elements with 4 and 5 parameters.
// Message length 131.
func Benchmark5424MassiveData(b *testing.B) {
	msg := "<190>1 2003-08-24T05:14:15.000003-07:00 san01-cluster.field-cluster.corporate-san.com squalidMessages " +
		"processID1234 0xab726ad723da5f4e " +
		"[exampleid1 \"a\"=\"abba\" \"class\"=\"massive-hyper-collider\" " +
		"\"location\"=\"Dr. Eggman's secret laboratory\" \"model\"=\"653.ab\"][exampleid2 \"b\"=\"baab\" " +
		"\"cluster\"=\"phoenix2\" \"driver\"=\"420.ax\" \"rack\"=\"32\" \"shelf\"=\"11\"] this sample message is " +
		"very long ... unnecessarily long actually. PLEASE STOP NOW. No, I won't, because this message has to be long"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark5424MaxDataNoStructuredDataNoMessage benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum Timestamp length.
// Maximum Hostname length (255).
// Maximum AppName length (48).
// Maximum ProcessID length (128).
// Maximum MessageID length (32).
// No structured data.
// No message.
func Benchmark5424MaxDataNoStructuredDataNoMessage(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, _ = Parse(parseTests5424[4].in)
	}
}

// Benchmark5424MaxDataNoStructuredDataNoMessage benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum Timestamp length.
// Maximum Hostname length (255).
// Maximum AppName length (48).
// Maximum ProcessID length (128).
// Maximum MessageID length (32).
// Multiple structured data elements with multiple structured data parameters. Maximum SD-NAME and PARAM-NAME length.
// No message.
func Benchmark5424MaxDataNoMessage(b *testing.B) {
	msg := "<191>1 2003-08-24T05:14:15.000003-07:00 " +
		// Hostname of length 255 characters.
		"12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com " +
		// Application name of length 48 characters.
		"abcdefghijklmnopqrstuvwxyz1234567890applications " +
		// Process id of length 128 characters.
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_" +
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_12345678901234567890 " +
		// Message id of length 32 characters.
		"MESSAGEID123456789_SECRETMESSAGE " +
		// Structured data.
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		// Message.
		"-"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark5424MaxDataNoStructuredData benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum Timestamp length.
// Maximum Hostname length (255).
// Maximum AppName length (48).
// Maximum ProcessID length (128).
// Maximum MessageID length (32).
// No structured data.
// Long message.
func Benchmark5424MaxDataNoStructuredData(b *testing.B) {
	msg := "<191>1 2003-08-24T05:14:15.000003-07:00 " +
		// Hostname of length 255 characters.
		"12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com " +
		// Application name of length 48 characters.
		"abcdefghijklmnopqrstuvwxyz1234567890applications " +
		// Process id of length 128 characters.
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_" +
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_12345678901234567890 " +
		// Message id of length 32 characters.
		"MESSAGEID123456789_SECRETMESSAGE " +
		// Structured data.
		"- " +
		// Message.
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum."
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark5424MaxData benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum Timestamp length.
// Maximum Hostname length (255).
// Maximum AppName length (48).
// Maximum ProcessID length (128).
// Maximum MessageID length (32).
// Multiple structured data elements with multiple structured data parameters. Maximum SD-NAME and PARAM-NAME lengths.
// Long message.
func Benchmark5424MaxData(b *testing.B) {
	msg := "<191>1 2003-08-24T05:14:15.000003-07:00 " +
		// Hostname of length 255 characters.
		"12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com " +
		// Application name of length 48 characters.
		"abcdefghijklmnopqrstuvwxyz1234567890applications " +
		// Process id of length 128 characters.
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_" +
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_12345678901234567890 " +
		// Message id of length 32 characters.
		"MESSAGEID123456789_SECRETMESSAGE " +
		// Structured data.
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		// Message.
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum."
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// ------------------------------
// ----- RFC3164 benchmarks -----
// ------------------------------

// Benchmark3164Minimal benchmarks parsing with the following message properties.
//
// No PRIVAL.
// No timestamp.
// Short hostname.
// No message.
func Benchmark3164Minimal(b *testing.B) {
	msg := "host"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark3164MinimalTimestamp benchmarks parsing with the following message properties.
//
// No PRIVAL.
// Timestamp.
// No hostname.
// No message.
func Benchmark3164MinimalTimestamp(b *testing.B) {
	msg := "Oct  1 13:52:40"
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// Benchmark3164MaxData benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length (3).
// Maximum Timestamp length.
// Maximum Hostname length (63).
// Long message.
func Benchmark3164MaxData(b *testing.B) {
	msg := "<191>Oct 11 22:14:15 " +
		// Host.
		"a_hostname_of_63_characters_that_complies_with_the_rfc1034_std_ " +
		// Message.
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum. " +
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore" +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum."
	for n := 0; n < b.N; n++ {
		_, _ = Parse(msg)
	}
}

// -------------------------------------
// ----- Octet counting benchmarks -----
// -------------------------------------

// Benchmark5424MaxDataNoStructuredData benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum Timestamp length.
// Maximum Hostname length (255).
// Maximum AppName length (48).
// Maximum ProcessID length (128).
// Maximum MessageID length (32).
// No structured data.
// Long message.
//
// Octet counting with two duplicate messages in one input string.
func Benchmark5424MaxDataNoStructuredDataOC(b *testing.B) {
	msg := "<191>1 2003-08-24T05:14:15.000003-07:00 " +
		// Hostname of length 255 characters.
		"12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com " +
		// Application name of length 48 characters.
		"abcdefghijklmnopqrstuvwxyz1234567890applications " +
		// Process id of length 128 characters.
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_" +
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_12345678901234567890 " +
		// Message id of length 32 characters.
		"MESSAGEID123456789_SECRETMESSAGE " +
		// Structured data.
		"- " +
		// Message.
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum."
	sb := strings.Builder{}
	for i1 := 0; i1 < 2; i1++ {
		sb.WriteString(strconv.Itoa(len(msg)))
		sb.WriteRune(' ')
		sb.WriteString(msg)
	}
	for n := 0; n < b.N; n++ {
		_, _ = Parse(sb.String())
	}
}

// Benchmark5424MaxDataOC benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length.
// Maximum Timestamp length.
// Maximum Hostname length (255).
// Maximum AppName length (48).
// Maximum ProcessID length (128).
// Maximum MessageID length (32).
// Multiple structured data elements with multiple structured data parameters. Maximum SD-NAME and PARAM-NAME lengths.
// Long message.
//
// Octet counting with two duplicate messages in one input string.
func Benchmark5424MaxDataOC(b *testing.B) {
	msg := "<191>1 2003-08-24T05:14:15.000003-07:00 " +
		// Hostname of length 255 characters.
		"12345678.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz." +
		"abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.abcdefghijklmnopqrstuvwxyz.com " +
		// Application name of length 48 characters.
		"abcdefghijklmnopqrstuvwxyz1234567890applications " +
		// Process id of length 128 characters.
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_ABCDEFGHIJKLMNOPQRSTUVWXYZ_" +
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ_12345678901234567890 " +
		// Message id of length 32 characters.
		"MESSAGEID123456789_SECRETMESSAGE " +
		// Structured data.
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		"[examplesd-id@thatis32characters! " +
		"example-param-name-thats-sd-name=\"short\" " +
		"example-param-name-thats-sd-name=\"short param value\" " +
		"example-param-name-thats-sd-name=\"longer parameter value than b4\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer even, basically\" " +
		"example-param-name-thats-sd-name=\"this parameter value is longer than the previous parameter values.\" " +
		"example-param-name-thats-sd-name=\"loooooongestest very long super not short parameter value, muuuuuch " +
		"longer than the previous longester parameter values, really looooooong\" " +
		"] " +
		// Message.
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum."
	sb := strings.Builder{}
	for i1 := 0; i1 < 2; i1++ {
		sb.WriteString(strconv.Itoa(len(msg)))
		sb.WriteRune(' ')
		sb.WriteString(msg)
	}
	for n := 0; n < b.N; n++ {
		_, _ = Parse(sb.String())
	}
}

// Benchmark3164MaxDataOC benchmarks parsing with the following message properties.
//
// Maximum PRIVAL length (3).
// Maximum Timestamp length.
// Maximum Hostname length (63).
// Long message.
//
// Octet counting with two duplicate messages in one input string.
func Benchmark3164MaxDataOC(b *testing.B) {
	msg := "<191>Oct 11 22:14:15 " +
		// Host.
		"a_hostname_of_63_characters_that_complies_with_the_rfc1034_std_ " +
		// Message.
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum. " +
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
		"dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip " +
		"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore" +
		"eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
		"deserunt mollit anim id est laborum."
	sb := strings.Builder{}
	for i1 := 0; i1 < 2; i1++ {
		sb.WriteString(strconv.Itoa(len(msg)))
		sb.WriteRune(' ')
		sb.WriteString(msg)
	}
	for n := 0; n < b.N; n++ {
		_, _ = Parse(sb.String())
	}
}

// Test3164Timestamp benchmarks parsing RFC3164 timestamp strings.
func Test3164Timestamp(t *testing.T) {
	for i1, e1 := range parseTests3164timestamps {
		l := lexer{
			input: e1.in,
			line:  1,
			end:   len(e1.in),
			state: lexTimestampRFC3164,
		}

		sr := SyslogRecord{}
		tok := l.nextToken()
		if err := parseTimestamp3164(tok, &l, &sr); err != nil {
			t.Fatalf("test %d: %s", i1+1, err)
		}
		if !e1.out.Equal(sr.Timestamp) {
			t.Fatalf("test %d: expected %q, got %q\n", i1+1, e1.out.String(), sr.Timestamp.String())
		}
		if tok = l.nextToken(); tok.typ != eof {
			t.Fatalf("test %d: expected end of input, got %s", i1+1, tok.String())
		}
	}
}
